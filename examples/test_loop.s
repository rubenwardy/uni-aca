#define i r0
#define size r1

main:
      mov i, #0
      mov size, #10

loop: add i, i, #1
      cmp i, size
      bnz loop
