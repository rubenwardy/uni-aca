.data

stack:
    .alloc 40  @ allocate 40 half words of stack

.text

#define x r0
#define y r1

main:
    mov x, #10
    mov sp, =stack
    mov lr, =done



factorial:                  @ int factorial(int x):
    cmp x, zr
    bnz factorial_nonzero   @     if x == 0:
    mov x, #1               @         x = 1;
    ret                @         return x;

factorial_nonzero:
    str x, sp, #0           @     y = x;
    add sp, sp, #2
    add x, x, #-1           @     x = x - 1;

    @ Call factorial
    str lr, sp, #0
    add sp, sp, #2
    mov lr, =factorial_call @ x = factorial(x);
    b factorial

factorial_call:
    add sp, sp, #-2
    ldr lr, sp, #0
    add sp, sp, #-2
    ldr y, sp, #0

    @ Multiply
    mul x, x, y             @     x = x * y;

    @ Return
    bz lr, #0               @     return x;



done:
    nop


