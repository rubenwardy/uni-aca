@ matrix multiplication

.data

A:
	.word 00000001
	.word 00000002
	.word 00000003
	.word 00000004
	.word 00000005
	.word 00000006
	.word 00000007
	.word 00000008
	.word 00000009

B:
	.word 00000009
	.word 00000008
	.word 00000007
	.word 00000006
	.word 00000005
	.word 00000004
	.word 00000003
	.word 00000002
	.word 00000001

C:
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000


stack:
    .alloc 4


.text

#define res r0
#define acc r1
#define xC r2
#define yC r3
#define i r4
#define idx1 r5
#define idx2 r6

#define size r7
#define num2 r8

#define val1 r9
#define val2 r10

    mov size, 3
    mov num2, 2
    mov sp, =stack
    mov xC, 0
    mov yC, 0

step:
    mov res, 0
    str lr, sp, 0
    add sp, sp, 2
    mov lr, =step_call_done
    b do_calc

step_call_done:
    add sp, sp, -2
    ldr lr, sp, 0

    mul idx1, yC, size
    add idx1, idx1, xC
    mul idx1, idx1, num2
    str res, idx1, =C


    add xC, xC, 1    @ xC++
    cmp xC, size
    bnz step         @ If xC >= size
    mov xC, 0        @     xC = 0
    add yC, yC, 1    @     yC++
    cmp yC, size     @     If yC >= size
    bz done          @         goto done
    b step

do_calc:
    mov i, 0

do_calc_loop:
    mul idx1, yC, size
    add idx1, idx1, i
    mul idx1, idx1, num2
    ldr val1, idx1, =A

    mul idx2, i, size
    add idx2, idx2, xC
    mul idx2, idx2, num2
    ldr val2, idx2, =B

    mul val1, val1, val2
    add res, res, val1

    add i, i, 1

    cmp i, size
    bnz do_calc_loop
    ret

done:
    nop


