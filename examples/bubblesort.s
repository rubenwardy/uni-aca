#define i r0
#define end r1
#define j r2
#define a r3
#define b r4
#define modified r5


@ Bubble sort!

.data

array:
	.word 00000010
	.word 00000005
	.word 00000004
	.word 00000003
	.word 00000012
	.word 00000022
	.word 00000001
	.word 00000000
	.word 00000017
	.word 00000002

.text
	mov end, #20       @ end_ptr = 20
	mov i, =array      @ i = 0
	add end, i, end    @ end_ptr = i + size

loopa:
	add j, i, #2       @ j = i + 2
	mov modified, #0   @ modified = 0
	ldr a, i, #0       @ Load current

loopb:
	ldr b, j, #0       @ Load next

	cmp a, b           @ Is current > next?
	bp swap
	b noswap

swap:
	str b, i, #0       @ Swap if wrong way around
	str a, j, #0
	mov modified, #1   @ modified = 1
	b check

noswap:
	mov a, b           @ current = next

check:
	mov i, j           @ i = j
	add j, j, #2       @ j = j + 2
	cmp j, end         @ Do we have more to do?
	bn loopb

	cmp modified, zr   @ Check if modified
	mov i, =array      @ i = 0
	bnz loopa
