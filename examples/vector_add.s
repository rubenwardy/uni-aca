@ vector addition

.data

A:
	.word 00000a10
	.word 00000005
	.word 00000004
	.word 00000203
	.word 00000012
	.word 0000a022
	.word 00000b01
	.word 0000c000
	.word 00000017
	.word 00000002

B:
	.word 00000010
	.word 00000005
	.word 00000504
	.word 00004003
	.word 00030012
	.word 00000022
	.word 00000301
	.word 00000000
	.word 00002017
	.word 00000102

C:
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000
	.word 00000000

.text

#define i r0
#define end r1
#define acc r2
#define tmp r3

    mov i, #0
    mov end, #20

step:
    ldr acc, i, =A

    ldr tmp, i, =B
    add acc, acc, tmp

    str acc, i, =C

    add i, i, #2
    cmp i, end
    bnz step


