#pragma once

#define COLOR_CLEAR "\033[0m"
#define COLOR_RED "\033[;31m"
#define COLOR_GREEN "\033[;32m"
#define COLOR_ORANGE "\033[;33m"
#define COLOR_BLUE "\033[;36m"
#define COLOR_GREEN_BG "\033[30;42m"
#define COLOR_BLUE_BG "\033[44m"
#define COLOR_RED_BOLD "\033[1;31m"

#define ERR(txt) std::cerr << COLOR_RED << txt << COLOR_CLEAR << std::endl
#define WARNING(txt) std::cerr << COLOR_ORANGE << txt << COLOR_CLEAR << std::endl

#include <cstdint>
#include <cmath>
#include <string>

typedef std::uint8_t  u8;
typedef std::uint16_t u16;
typedef std::uint32_t u32;
typedef std::uint64_t u64;

typedef signed char      s8;
typedef signed short     s16;
typedef signed int       s32;
typedef signed long long s64;

typedef unsigned int uint;

typedef float  f32;
typedef double f64;

#include <algorithm>

static inline void ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
		return !std::isspace(ch);
	}));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
		return !std::isspace(ch);
	}).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
	ltrim(s);
	rtrim(s);
}
