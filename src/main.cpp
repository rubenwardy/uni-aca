#include <iostream>
#include "src/asm/Assembler.hpp"
#include "src/vm/VM.hpp"
#include "tests/tests.hpp"

#ifdef ENABLE_GUI
	#include "src/vm/Debugger.hpp"
#endif

#include <unistd.h>
#include <string.h>

using namespace ASM;

void printUsage() {
	std::cerr << "Usage: aca [--cli] [filepath]" << std::endl;
}

int main(int n, char **args) {
	if (!run_tests()) {
		return 1;
	}

	std::string filepath = "examples/matrix_mult.s";
#ifdef ENABLE_GUI
	bool debugger = true;
#endif
	bool assemble_only = false;
	VM::FeatureFlags flags = VM::FeatureFlags();

	{
		bool filepath_seen = false;
		for (int i = 1; i < n; i++) {
			if (strncmp(args[i], "--", 2) == 0) {
				std::string arg(args[i]);
				if (arg == "--cli") {
#ifdef ENABLE_GUI
					debugger = false;
#endif
				} else if (arg == "--asm") {
					assemble_only = true;
				} else if (arg == "--simple") {
					flags.pipelining = false;
					flags.super      = false;
				} else if (arg == "--pipelined") {
					flags.pipelining = true;
					flags.super      = false;
				} else if (arg == "--super" || arg == "--superscalar" || arg == "--super-scalar") {
					flags.pipelining = true;
					flags.super = true;
				} else if (arg == "--no-bp") {
					flags.speculative = false;
				} else if (arg == "--never-bp") {
					flags.speculative = true;
					flags.bp_type = VM::FeatureFlags::NEVER_TAKE;
				} else if (arg == "--always-bp") {
					flags.speculative = true;
					flags.bp_type = VM::FeatureFlags::ALWAYS_TAKE;
				} else if (arg == "--static-bp") {
					flags.speculative = true;
					flags.bp_type = VM::FeatureFlags::STATIC;
				} else if (arg == "--dynamic-bp") {
					flags.speculative = true;
					flags.bp_type = VM::FeatureFlags::DYNAMIC;
				} else {
					std::cerr << "Unknown flag: " << arg << std::endl;
					printUsage();
					return (arg == "--help") ? 0 : 1;
				}
			} else if (args[i][0] == '-') {
				for (int j = 1; j < strlen(args[i]); j++) {
					switch (args[i][j]) {
					case 'a':
						assemble_only = true;
						break;

					case 'c':
#ifdef ENABLE_GUI
						debugger = false;
#endif
						break;

					case 'h':
						printUsage();
						return 0;

					case 'p':
						flags.pipelining = true;
						flags.super      = false;
						break;

					case 's':
						flags.pipelining = false;
						flags.super      = false;
						break;

					case 'r':
						flags.pipelining = true;
						flags.super      = true;
						break;

					default:
						std::cerr << "Unknown flag: " << args[i][j] << std::endl;
						printUsage();
						return 1;
					}
				}
			} else if (filepath_seen) {
				printUsage();
				return 1;
			} else {
				filepath_seen = true;
				filepath = args[i];
			}
		}
	}

	Assembler as;
	if (!as.parseFile(filepath)) {
		return 1;
	}

	if (assemble_only) {
		for (auto inst : as.getInstructions()) {
			printf("%.8x\n", inst);
		}

		return 0;
	}

	VM::VM vm(flags);
	vm.setInstructions(as.getInstructions());

#ifdef ENABLE_GUI
	if (debugger) {
		auto app = Gtk::Application::create("com.rubenwardy.aca");
		app->set_accel_for_action("app.quit", "Escape");
		app->set_accel_for_action("app.step", "s");
		app->set_accel_for_action("app.stop", "r");
		app->set_accel_for_action("app.load", "<Primary>o");
		app->set_accel_for_action("app.togglerun", "space");
		VM::Debugger db(&vm);
		return app->run(db);

	} else
#endif
		{
		while (vm.step()) {
			vm.printDebug();
		}
		vm.printDump();

		return 0;
	}
}
