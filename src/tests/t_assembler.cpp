#include "t_assembler.hpp"
#include "tests.hpp"
#include "src/asm/Assembler.hpp"

using namespace ASM;

#include <iostream>

#define TESTI(exp, op, ...) {                         \
	u32 inst = as.parseInst((op), { __VA_ARGS__ });   \
	/*std::cerr << std::hex << inst << std::endl;*/   \
	TEST(inst == exp);                                \
}

bool parseInstTests() {
	Assembler as;

	TESTI(0x00000000, NOP)
	TESTI(0x10120000, ADD, "r0", "r1", "r2")
	TESTI(0x19870000, ADD, "r9", "r8", "r7")
	TESTI(0x29800064, ADD, "r9", "r8", "#100")
	TESTI(0x29800064, ADD, "r9", "#100", "r8")
	TESTI(0x29800064, ADD, "r9", "r8", "100")
	TESTI(0x298FFFFF, ADD, "r9", "r8", "#-1")
	TESTI(0x20100000, MOV, "r0", "r1")
	TESTI(0x29800000, MOV, "r9", "r8")
	TESTI(0x22C0000F, MOV, "r2", "#15")
	TESTI(0x2FE00000, MOV, "pc", "lr")
	TESTI(0x39500000, CMP, "r9", "r5")
	as.insertLabel("label", 0);
	TESTI(0x41400000, LDR, "r1", "r4", "#0")
	TESTI(0x44100064, LDR, "r4", "r1", "#100")
	TESTI(0x51400000, STR, "r1", "r4", "#0")
	TESTI(0x51400064, STR, "r1", "r4", "#100")
	TESTI(0x60120000, MUL, "r0", "r1", "r2")
	TESTI(0x69870000, MUL, "r9", "r8", "r7")
	TESTI(0x70120000, DIV, "r0", "r1", "r2")
	TESTI(0x79870000, DIV, "r9", "r8", "r7")
	TESTI(0xB0C00000, B,   "label")
	TESTI(0xB1C00000, BZ,  "label")
	TESTI(0xB2C00000, BNZ, "label")
	TESTI(0xB3C00000, BP,  "label")
	TESTI(0xB4C00000, BN,  "label")
	TESTI(0xB0E00000, B,   "lr", "label")
	TESTI(0xB1E00000, BZ,  "lr", "label")
	TESTI(0xB2E00000, BNZ, "lr", "label")
	TESTI(0xB0E00000, RET)
	TESTI(0xB3300000, BP,  "r3", "label")
	TESTI(0xB4B00000, BN,  "r11", "label")
	TESTI(0xB4B00022, BN,  "r11", "34")
	TESTI(0xB4BFFFFF, BN,  "r11", "-1")

	return true;
}


bool run_assembler_tests() {
	return parseInstTests();
}
