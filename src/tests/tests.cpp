#include "tests.hpp"
#include "t_assembler.hpp"
#include "../utils.hpp"

#include <iostream>

bool run_tests() {
	bool success = true;

	success &= run_assembler_tests();

	if (success) {
		std::cerr << COLOR_GREEN "All tests pass!" COLOR_CLEAR << std::endl;
	}

	return success;
}
