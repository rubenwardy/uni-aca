#pragma once

#include "../utils.hpp"

#define TEST(x) if (!(x)) { \
	std::cerr << COLOR_RED "Test " #x " failed at " << __FILE__ << ":" << __LINE__ << COLOR_CLEAR << std::endl; \
	return false; }

bool run_tests();
