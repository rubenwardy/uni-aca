#include "Parser.hpp"

#include <iostream>
#include <sstream>

using namespace ASM;

bool Parser::parse(std::istream &iss) {
	bool success = true;

	std::string line;
	int lineno = 0;
	while (std::getline(iss, line)) {
		lineno++;

		line = line.substr(0, line.find('@', 0));
		trim(line);

		try {
			parseLine(lineno, line);
		} catch (ParseException &e) {
			ERR("Error on line " << lineno << ": " << e.info);
			success = false;
		}
	}

	if (!labels_open.empty()) {
		ERR("Error on line " << lineno <<  ": orphan labels");
	}

	return success;
}

void Parser::parseLine(int lineno, std::string line) {
	// Trim and read line
	std::istringstream iss(line);

	// Read cmd and discard space
	std::string cmd;
	std::getline(iss, cmd, ' ');

	if (cmd == "#define") {
		std::string params[2];
		for (int i = 0; i < 2; i++) {
			if (iss.eof()) {
				ERRPE("#define needs at least two params");
			}
			std::getline(iss, params[i], ' ');
			trim(params[i]);
		}

		// Fail on remaining string
		if (!iss.eof()) {
			std::string remaining;
			getline(iss, remaining);
			ERRPE("Expected EOL, not \"" << remaining << "\"");
		}

		replacements[params[0]] = params[1];
		return;
	}

	if (cmd == ".data" || cmd == ".text") {
		Mode next = (cmd == ".data") ? DATA : TEXT;
		if (mode != next && !instructions.empty()) {
			do {
				instructions.push_back({ NOP, {}, lineno });
			} while ((instructions.size() % 0x10) != 0);
		}

		if (next == DATA && instructions.empty()) {
			parseLine(lineno, "b __text");
			instructions.push_back({ NOP, {}, lineno });
		} else if (next == TEXT) {
			parseLine(lineno, "__text:");
		}

		mode = next;

		if (iss.eof()) {
			cmd = "";
		} else {
			std::getline(iss, cmd, ' ');
		}
	}

	// Check for label
	if (!cmd.empty() && cmd[cmd.size() - 1] == ':') {
		std::string label = cmd.substr(0, cmd.size() - 1);
		if (iss.eof()) {
			cmd = "";
		} else {
			std::getline(iss, cmd, ' ');
		}

		if (std::find(labels_open.begin(), labels_open.end(), label) != labels_open.end() ||
				labels.find(label) != labels.end()) {
			ERRPE("Label " << label << " already defined");
		}

		labels_open.push_back(label);
	}

	if (!cmd.empty()) {
		AsmOp op;
		if (!opFromName(op, cmd)) {
			ERRPE("Unknown op: " << cmd);
		}

		// Read operands
		std::vector<std::string> params;
		while (!iss.eof()) {
			params.push_back("");
			std::string &part = params[params.size() - 1];
			std::getline(iss, part, ',');
			trim(part);

			auto rep = replacements.find(part);
			if (rep != replacements.end()) {
				part = rep->second;
			}
		}

		// Map labels
		for (const auto &label : labels_open) {
			labels[label] = instructions.size() ;
		}
		labels_open.clear();


		// Build instructions
		if (op == WORD) {
			if (params.size() != 1) {
				ERRPE("Excepted 1 parameter to .word");
			}

			u64 data;

			std::istringstream ss(params[0]);
			ss >> std::hex >> data;

			// HACK: This is a hack!
			instructions.push_back({INST, {std::to_string(data >> 32)}, lineno});
			instructions.push_back({INST, {std::to_string(data & 0xFFFFFFFF)}, lineno});
		} else if (op == ALLOC) {
			u64 size;
			std::istringstream ss(params[0]);
			ss >> size;

			for (u64 i = 0; i < size; i++) {
				instructions.push_back({INST, { "0" }, lineno});
			}
		} else {
			instructions.push_back({ op, params, lineno });
		}
	}

	// Fail on remaining string
	if (!iss.eof()) {
		std::string remaining;
		getline(iss, remaining);

		ERRPE("Expected EOL, not \"" << remaining << "\"");
	}
}



bool ASM::opFromName(AsmOp &op, std::string cmd) {
	std::transform(cmd.begin(), cmd.end(), cmd.begin(), ::tolower);

	if (cmd == "nop") {
		op = NOP;
		return true;
	} else if (cmd == "mov") {
		op = MOV;
		return true;
	} else if (cmd == "add") {
		op = ADD;
		return true;
	} else if (cmd == "cmp") {
		op = CMP;
		return true;
	} else if (cmd == "ldr") {
		op = LDR;
		return true;
	} else if (cmd == "str") {
		op = STR;
		return true;
	} else if (cmd == "b") {
		op = B;
		return true;
	} else if (cmd == "bz") {
		op = BZ;
		return true;
	} else if (cmd == "bnz") {
		op = BNZ;
		return true;
	} else if (cmd == "bp") {
		op = BP;
		return true;
	} else if (cmd == "bn") {
		op = BN;
		return true;
	} else if (cmd == "mul") {
		op = MUL;
		return true;
	} else if (cmd == "div") {
		op = DIV;
		return true;
	} else if (cmd == "ret") {
		op = RET;
		return true;
	} else if (cmd == ".word") {
		op = WORD;
		return true;
	} else if (cmd == ".inst") {
		op = INST;
		return true;
	} else if (cmd == ".alloc") {
		op = ALLOC;
		return true;
	} else {
		return false;
	}
}
