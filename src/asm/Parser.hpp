#pragma once

#include <vector>
#include <istream>
#include <map>

#include "../utils.hpp"
#include "types.hpp"

namespace ASM {

extern bool opFromName(AsmOp &op, std::string cmd);

class Parser {
	std::vector<std::string> labels_open;
	std::map<std::string, std::string> replacements;

	enum Mode {
		TEXT = 0,
		DATA
	};
	Mode mode = TEXT;

public:
	std::map<std::string, int> labels;
	std::vector<AsmInstruction> instructions;

	bool parse(std::istream &iss);
	void parseLine(int lineno, std::string line);
};

};
