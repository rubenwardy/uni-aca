#pragma once

#include <istream>
#include <exception>
#include <vector>
#include <list>
#include <string>
#include <map>
#include <algorithm>
#include <set>

#include "../utils.hpp"
#include "types.hpp"
#include "Parser.hpp"

namespace ASM {

class Assembler {
	std::vector<u32> instructions;
	std::map<std::string, int> labels;
	std::set<std::string> unused_labels;

	Parser parser;

public:
	bool parseFile(std::string path);
	bool parse(std::istream &infile, const std::string &path);

	u32 parseInst(const AsmOp &op, const std::vector<std::string> &params);
	Token parseToken(const std::string &str);
	void addInstruction(u32 inst);
	u32 findLabel(const std::string &label);

	std::vector<u32> getInstructions() {
		return instructions;
	}

	void insertLabel(const std::string &label, int pos) {
		labels[label] = pos;
	}
};

};
