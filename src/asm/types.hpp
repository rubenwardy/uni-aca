#include <utility>

#pragma once

namespace ASM {

enum AsmOp {
	NOP = 0,
	ADD,
	ADDI,
	CMP,
	LDR,
	STR,
	MUL,
	DIV,
	B = 0xB,
	BZ,
	BNZ,
	BP,
	BN,
	MOV,
	WORD,
	INST,
	ALLOC,
	RET,
};

struct AsmInstruction {
	AsmOp op;
	std::vector<std::string> params;
	int lineno;
};

struct Token {
	bool is_reg = true;
	u32 value = 0;
};

class ParseException : public std::exception {
public:
	std::string info;

	explicit ParseException(std::string info):
		info(std::move(info))
	{}

	const char *what() const noexcept override;
};

#define ERRPE(txt) { std::ostringstream os; os << __FILE__ << ":" << __LINE__ << " " << txt; throw ParseException(os.str()); }
#define ERROP(exp, size) ERRPE("Expected " << exp                          \
		<< " operands, got " << size);


#define CHECK_OP_RANGE(min, max)                            \
	if (params.size() < (min) || params.size() > (max)) {   \
		ERROP(exp, params.size());                          \
	}

#define CHECK_OP(exp) CHECK_OP_RANGE(exp, exp)

};
