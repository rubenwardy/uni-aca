#include "Assembler.hpp"
#include "../isa/isa.hpp"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace ASM;

const char *ParseException::what() const noexcept {
	std::ostringstream os;
	os << "Error on line ?: " << info;
	std::cerr << os.str() << std::endl;
	return os.str().c_str();
}

bool Assembler::parseFile(std::string path) {
	std::ifstream infile(path);
	if (!infile.good()) {
		ERR("Unable to find file " << path);
		return false;
	}

	return parse(infile, path);
}

bool Assembler::parse(std::istream &infile, const std::string &path) {
	if (!parser.parse(infile)) {
		return false;
	}

	bool success = true;
	labels = parser.labels;
	for (auto label : labels) {
		unused_labels.insert(label.first);
	}

	for (auto inst : parser.instructions) {
		try {
			u32 code = parseInst(inst.op, inst.params);
			addInstruction(code);
		} catch (ParseException &e) {
			ERR("Error at " << path << ":" << std::dec << inst.lineno << ": " << e.info);
			success = false;
		}
	}

	for (auto label : unused_labels) {
		WARNING("Unused label " << label);
	}

	return success;
}

u32 Assembler::parseInst(const AsmOp &op, const std::vector<std::string> &params) {
	ISA::Inst inst;

	switch (op) {
	case NOP:
		inst.op = ISA::NOP;
		CHECK_OP(0);
		break;

	case MOV: {
		CHECK_OP(2);

		Token t1 = parseToken(params[0]);
		Token t2 = parseToken(params[1]);

		if (!t1.is_reg) {
			ERRPE("First operand to MOV must be a register");
		}

		inst.op = ISA::ADDI;

		if (t2.is_reg) {
			inst.a = t1.value;
			inst.b = t2.value;
		} else {
			if (!ISA::Inst::CheckIMM(t2.value)) {
				ERRPE("Intermediate out of range");
			}

			inst.a = t1.value;
			inst.b = 0xC;
			inst.imm = t2.value;
		}
		break;
	}

	case ADD:
	case MUL:
	case DIV: {
		CHECK_OP(3);

		Token t1 = parseToken(params[0]);
		Token t2 = parseToken(params[1]);
		Token t3 = parseToken(params[2]);

		if (!t1.is_reg) {
			ERRPE("ADD: Target register must be a register");
		}

		if (!t2.is_reg && !t3.is_reg) {
			ERRPE("ADD: No support for two intermediates");
		}

		inst.a = t1.value;
		if (t2.is_reg && t3.is_reg) {
			inst.op = (ISA::Op)op;
			inst.b  = t2.value;
			inst.c  = t3.value;
		} else {
			if (op != ADD) {
				ERRPE("MUL/DIV: No support for intermediates");
			}

			if (t3.is_reg) {
				std::swap(t2, t3);
			}

			inst.op = ISA::ADDI;

			if (!ISA::Inst::CheckIMM(t3.value)) {
				ERRPE("Intermediate out of range");
			}

			inst.b = t2.value;
			inst.imm = t3.value;
		}

		break;
	}

	case CMP: {
		CHECK_OP(2);

		Token t1 = parseToken(params[0]);
		Token t2 = parseToken(params[1]);

		if (!t1.is_reg || !t2.is_reg) {
			ERRPE("CMP does not accept intermediates");
		}

		inst.op = ISA::CMP;
		inst.a  = t1.value;
		inst.b  = t2.value;

		break;
	}

	case LDR:
	case STR: {
		CHECK_OP(3);

		Token t1 = parseToken(params[0]);
		Token t2 = parseToken(params[1]);
		Token t3 = parseToken(params[2]);

		if (!t1.is_reg || !t2.is_reg) {
			ERRPE("LDR/STR must have a target and base register");
		}

		if (t3.is_reg) {
			ERRPE("LDR/STR must have an offset immediate");
		}

		if (!ISA::Inst::CheckIMM(t3.value)) {
			ERRPE("Intermediate out of range");
		}


		inst.op  = (op == LDR) ? ISA::LDR : ISA::STR;
		inst.a   = t1.value;
		inst.b   = t2.value;
		inst.imm = t3.value;

		break;
	}

	case B:
	case BZ:
	case BNZ:
	case BP:
	case BN: {
		CHECK_OP_RANGE(1, 2);

		inst.op = ISA::B;
		inst.a  = op - B;
		inst.b  = ISA::rZR;

		std::string reg, imm;
		if (params.size() == 2) {
			reg = params[0];
			imm = params[1];
		} else {
			imm = params[0];
		}

		if (!reg.empty()) {
			Token token = parseToken(reg);
			if (!token.is_reg) {
				ERRPE("Branch(2): param 1 needs to be a register");
			}

			if (token.value & 0xFFFFFFF0) {
				ERRPE("Branch(2): param 1 is out of bounds");
			}

			inst.b = (char)token.value;
		}

		inst.imm = findLabel(imm);
		if (inst.imm == (u32)-1) {
			Token token;
			try {
				token = parseToken(imm);
			} catch (const ParseException &e) {
				ERRPE("Branch: Destination " << imm << " is not a label or a token." <<  e.what());
			}

			if (token.is_reg) {
				ERRPE("Branch: Destination needs to be a immediate");
			}

			inst.imm = token.value;
		}

		if (!ISA::Inst::CheckIMM(inst.imm)) {
			ERRPE("Branch: Immediate out of range");
		}

		break;
	}

	case INST: {
		CHECK_OP(1);

		Token t1 = parseToken(params[0]);

		if (t1.is_reg) {
			ERRPE("Expecting data for .INST/WORD");
		}

		if (t1.value > 0xFFFFFFFF) {
			ERRPE(".word/.inst too large");
		}

		return t1.value;
	}

	case RET: {
		CHECK_OP(0);

		inst.op = ISA::B;
		inst.a = 0;
		inst.b = ISA::rLR;
		inst.imm = 0;
		break;
	}

	default:
		ERRPE("Compiler error: unknown opcode " << op)
		break;
	}

	return inst.encode();
}

Token Assembler::parseToken(const std::string &str) {
	if (str.empty()) {
		ERRPE("Argument is empty (must be of form r0-r28, sp, lr, pc, #[0-9]+)");
	}

	if (str[0] == '#' || isdigit(str[0]) || str[0] == '-') {
		std::istringstream iss(str);
		if (str[0] == '#') {
			iss.ignore(1);
		}

		bool isNegative = false;
		if (iss.peek() == '-') {
			iss.ignore(1);
			isNegative = true;
		}

		u32 val;
		iss >> val;

		if (!iss.eof()) {
			std::string remaining;
			getline(iss, remaining);
			ERRPE("Expected ',' got " << remaining);
		}

		if (isNegative) {
			val = ~val + 1;
		}

		return { false, val };

	} else if (str[0] == 'r') {
		std::istringstream iss(str);
		iss.ignore(1);

		int val;
		iss >> val;

		if (val >= 16) {
			ERRPE("Registers must be in range r0-r15");
		}

		if (!iss.eof()) {
			std::string remaining;
			getline(iss, remaining);
			ERRPE("Expected ',' got " << remaining);
		}

		return { true, val };

	} else if (str[0] == '=') {
		std::istringstream iss(str);
		iss.ignore(1);

		std::string label;
		std::getline(iss, label);

		auto found = labels.find(label);
		if (found == labels.end()) {
			ERRPE("Use of undefined label " << label);
		}

		unused_labels.erase(label);

		return { false, found->second };

	} else if (str == "zr") {
		return { true, 0xC };

	} else if (str == "sp") {
		return { true, 0xD };

	} else if (str == "lr") {
		return { true, 0xE };

	} else if (str == "pc") {
		return { true, 0xF };
	}

	ERRPE("Argument must be of form r0-r28, sp, lr, pc, #[0-9A-F]+");
}

void Assembler::addInstruction(u32 inst) {
	instructions.push_back(inst);
}

u32 Assembler::findLabel(const std::string &label) {
	auto found = labels.find(label);
	if (found == labels.end()) {
		return (u32)-1;
	}
	unused_labels.erase(label);

	if (found->second & 0xFFF00000) {
		ERRPE("Compiler error: Label offset out of range");
	}

	return found->second;
}
