#pragma once

#include "VM.hpp"

#include <gtkmm.h>

namespace VM {

class Debugger : public Gtk::Window {
public:
	explicit Debugger(VM *vm);
//	~Debugger() override = default;

	void resume();
	void step();
	void pause();
	void stop();
	bool update();
	void displayState();
	void createInstRows();
	void createMemoryRows();
	void updateMemoryRows();
	void onInstSelectionChanged();
	void onLoadClicked();
	bool onClockChanged(Gtk::ScrollType, double);
	void onToggleRun() { if (running) pause(); else resume(); }
	void quit() { exit(0); }
	void doVMStep();

protected:
	Glib::RefPtr<Gio::SimpleActionGroup> tbActionGroup;
	Gtk::Toolbar* toolbar = nullptr;
	Gtk::Box info, bxClock;
	Gtk::Paned split, split2;
	Gtk::Frame fRegs, fClock, fPipeline;
	Gtk::Label lRegs, lTick, lPipeline;
	Gtk::Scale sClock;
	Glib::RefPtr<Gtk::Builder> refBuilder;
	bool running = false;
	VM *vm;
	sigc::connection tick_conn;
	int numMemoryRows = 0;

	class InstructionColumns : public Gtk::TreeModel::ColumnRecord {
	public:
		Gtk::TreeModelColumn<unsigned int> pc;
		Gtk::TreeModelColumn<u32> inst;
		Gtk::TreeModelColumn<Glib::ustring> op;
		Gtk::TreeModelColumn<Glib::ustring> operands;
		Gtk::TreeModelColumn<Glib::ustring> color;

		InstructionColumns() {
			add(pc);
			add(inst);
			add(op);
			add(operands);
			add(color);
		}
	};

	InstructionColumns colsInstructions;

	class MemoryColumns : public Gtk::TreeModel::ColumnRecord {
	public:
		Gtk::TreeModelColumn<unsigned int> address;
		Gtk::TreeModelColumn<u32> dataA;
		Gtk::TreeModelColumn<u32> dataB;
		Gtk::TreeModelColumn<Glib::ustring> color;

		MemoryColumns() {
			add(address);
			add(dataA);
			add(dataB);
			add(color);
		}
	};

	MemoryColumns colsMemory;


	Gtk::ScrolledWindow scrollInstructions, scrollMemory;
	Gtk::TreeView treeViewInstructions, treeViewMemory;
	Glib::RefPtr<Gtk::ListStore> refTreeModelInstructions, refTreeModelMemory;
	bool isMemoryColored = false;
};

};
