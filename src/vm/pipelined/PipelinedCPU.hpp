#pragma once

#include "../../utils.hpp"
#include "../../isa/isa.hpp"
#include "../types.hpp"
#include "../ICPU.hpp"
#include "../PipelineStage.hpp"
#include "../stages/stages.hpp"

using ISA::Inst;

namespace VM {

class VM;
class PipelinedCPU : public ICPU, IPipelineAPI {
public:
	FetchStage   fetch   = FetchStage(this);
	DecodeStage  decode  = DecodeStage(this);
	ExecuteStage execute = ExecuteStage(this);
	WriteStage   write   = WriteStage(this);

	explicit PipelinedCPU(VM *vm):
		ICPU(vm)
	{
		clear();
	}

	void clear() override;
	void clearExceptWrite();
	bool isRunnable() const override;
	void step() override;
	std::string getCPUName() const override { return "Pipelined"; }

	u32 getInstruction(u64 address) const override;
	u64 readMemory(u64 address) override;
	void writeMemory(u64 address, u64 value) override;
	u64 getR(int id) override;
	void setR(int id, u64 val) override;

	bool hasMarks() const override { return true; }
	std::vector<Mark> getMarks() const override;
};

};
