#include "PipelinedCPU.hpp"
#include "src/vm/VM.hpp"

#include <iostream>

using namespace VM;
using namespace ISA;

void PipelinedCPU::clear() {
	fetch.resetValue();
	decode.resetValue();
	execute.resetValue();
	write.resetValue();
}

void PipelinedCPU::clearExceptWrite() {
	fetch.resetValue();
	decode.resetValue();
	execute.resetValue();
}

bool PipelinedCPU::isRunnable() const {
	return vm->peekRegisterFile().getRaw(rPC) < vm->peekInstructions().size() + 4;
}

void PipelinedCPU::step() {
	u64 oldPC = getR(rPC);

	// Writeback
	if (!InterlockStep<WBD, int, void*>(&write, nullptr)){
		return;
	}

	// Clear pipeline
	if (write.getValue().rID == rPC) {
		if (write.getValue().rV == write.getPC() + 1) {
			std::cerr << "Branch predict successful" << std::endl;
			vm->notifyBranch(true);
		} else {
			std::cerr << "Branch predict error, clearing pipeline" << std::endl;
			clear();
			fetch.setValue(getR(rPC));
			vm->notifyBranch(false);
			return;
		}
	}

	// Reset writeback to avoid dup writes
	write.resetValue();

	// Execute
	WBD wbd;
	if (!InterlockStep<ResolvedInst, WBD, int>(&execute, &write, &wbd)) {
		execute.incRetries();
		return;
	} else if (execute.getValue().inst.op != NOP) {
		vm->notifyInstComplete();
	}

	// Decode
	if (!InterlockStep<u32, ResolvedInst, WBD>(&decode, &execute)) {
		return;
	}

	// Bypass
	auto read_rIDs = execute.getValue().inst.getReadRegisters();
	auto res = execute.getValue();
	if (read_rIDs[0] > -1 && read_rIDs[0] == wbd.rID) {
		res.read1val = wbd.rV;
	} else if (read_rIDs[1] > -1 && read_rIDs[1] == wbd.rID) {
		res.read2val = wbd.rV;
	}
	execute.setValue(res);

	// Fetch
	if (InterlockStep<u64, u32, ResolvedInst>(&fetch, &decode)) {
		setR(rPC, getR(rPC) + 1);
		fetch.setValue(getR(rPC));
	}
}

u32 PipelinedCPU::getInstruction(u64 address) const {
	auto &instructions = vm->getInstructions();
	return (address >= instructions.size()) ? 0 : instructions[address];
}

u64 PipelinedCPU::readMemory(u64 address) {
	return vm->readMemory(address);
}

void PipelinedCPU::writeMemory(u64 address, u64 value) {
	vm->writeMemory(address, value);
}

u64 PipelinedCPU::getR(int id) {
	auto &file = vm->getRegisterFile();
	return file.get(id);
}

void PipelinedCPU::setR(int id, u64 val) {
	auto &file = vm->getRegisterFile();
	file.set(id, val);
}

std::vector<ICPU::Mark> PipelinedCPU::getMarks() const {
	std::vector<Mark> marks;
	marks.emplace_back("Fetch", fetch.getPCSigned(), fetch.toString(), "#00bcd4");
	marks.emplace_back("Decode", decode.getPCSigned(), decode.toString(), "#0082d4");
	marks.emplace_back("Fetch", execute.getPCSigned(), execute.toString(), "#663cd4");
	marks.emplace_back("Write", write.getPCSigned(), write.toString(), "#b34040");
	return marks;
}
