#pragma once

#include "../ResolvedInst.hpp"
#include "../../utils.hpp"
#include "../../isa/isa.hpp"
#include "../PipelineStage.hpp"

using ISA::Inst;

class FetchStage : public PipelineStage<u64, u32> {
	u64 address = 0;
public:
	explicit FetchStage(IPipelineAPI *pipeline): PipelineStage(pipeline) {}

	void setValue(u64 input) override { address = input; setPC(input); }
	u64  getValue() const override    { return address;  }
	void resetValue() override        { address = 0; pc = 0; }
	bool execute(u32 &out) const override;

	std::string toString() const override;
};

class DecodeStage : public PipelineStage<u32, ResolvedInst> {
	u32 machine = 0;
public:
	explicit DecodeStage(IPipelineAPI *pipeline): PipelineStage(pipeline) {}

	void setValue(u32 input) override { machine = input; setPC(input); }
	u32  getValue() const override    { return machine;  }
	void resetValue() override        { machine = 0; pc = (u64)-1; }
	bool execute(ResolvedInst &out) const override;

	std::string toString() const override;
};

struct WBD {
	int rID   = -1;
	u64 rV    = 0;

	std::string toString() const;

	void reset() {
		rID = -1;
	}
};

bool shouldTakeBranch(Inst inst, u64 cond);

class ExecuteStage : public PipelineStage<ResolvedInst, WBD> {
	ResolvedInst resolved;
	int retries;

public:
	explicit ExecuteStage(IPipelineAPI *pipeline): PipelineStage(pipeline) {}

	void setValue(ResolvedInst input) override { resolved = input; retries = 0; }
	ResolvedInst getValue() const override     { return resolved; }
	void resetValue() override { resolved = ResolvedInst(); pc = (u64)-1; retries = 0; }
	bool execute(WBD &out) const override;

	void incRetries() { retries++; }

	std::string toString() const override;

};

class WriteStage : public PipelineStage<WBD, int> {
	WBD wbd;

public:
	explicit WriteStage(IPipelineAPI *pipeline): PipelineStage(pipeline) {}

	void setValue(WBD input) override { wbd = input; }
	WBD  getValue() const override    { return wbd; }
	void resetValue() override        { wbd.reset(); pc = (u64)-1; }
	bool execute(int &out) const override;

	std::string toString() const override;
};
