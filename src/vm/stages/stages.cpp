#include "stages.hpp"

#include <iostream>
#include <sstream>
#include <src/vm/types.hpp>

using namespace ISA;

bool FetchStage::execute(u32 &out) const {
	out = pipeline->getInstruction(address);
	return true;
}

std::string FetchStage::toString() const {
	std::ostringstream os;
	os << std::hex;
	os << address;
	return os.str();
}

bool DecodeStage::execute(ResolvedInst &out) const {
	out = ResolvedInst();
	out.inst = Inst(machine);

	auto read_rIDs = out.inst.getReadRegisters();
	out.read1val = (read_rIDs[0] > -1) ? pipeline->getR(read_rIDs[0]) : out.inst.a;
	out.read2val = (read_rIDs[1] > -1) ? pipeline->getR(read_rIDs[1]) : out.inst.b;

	return true;
}

std::string DecodeStage::toString() const {
	std::ostringstream os;
	os << std::hex;
	os << machine;
	return os.str();
}

bool ExecuteStage::execute(WBD &out) const {
	std::cerr << "[Execute] " << resolved.toString() << std::endl;

	out.reset();

	Inst inst = resolved.inst;
	switch (inst.op) {
	case NOP:
		break;

	case ADD:
		out.rID = inst.a;
		out.rV  = resolved.read1val + resolved.read2val;
		break;

	case ADDI:
		out.rID = inst.a;
		out.rV  = resolved.read1val + inst.getIMM64();
		break;

	case CMP: {
		out.rID = rCOND;

		u64 a = resolved.read1val;
		u64 b = resolved.read2val;
		if (a == b) {
			out.rV = 0;
		} else if (a > b) {
			out.rV = 1;
		} else if (a < b) {
			out.rV = 2;
		}
		break;
	}

	case LDR: {
		vassert(pipeline, "Attempt to run LDR op on ALU", getPCSigned());

		if (retries < LOAD_CYCLES) {
			return false;
		}

		u64 address = resolved.read1val + inst.getIMM64();
		out.rID = inst.a;
		out.rV  = pipeline->readMemory(address);
		break;
	}

	case STR: {
		vassert(pipeline, "Attempt to run STR op on ALU", getPCSigned());

		if (retries < STORE_CYCLES) {
			return false;
		}

		u64 address = resolved.read1val + inst.getIMM64();
		pipeline->writeMemory(address, resolved.read2val);
		break;
	}

	case MUL:
		out.rID = inst.a;
		out.rV  = resolved.read1val * resolved.read2val;
		break;

	case DIV:
		out.rID = inst.a;
		out.rV  = resolved.read1val / resolved.read2val;
		break;

		// case BKPT:


	case B: {
		if (shouldTakeBranch(inst, resolved.read1val)) {
			std::cerr << "BRANCHING!" << std::endl;
			out.rID = rPC;
			out.rV  = resolved.read2val + inst.getIMM64();
		}
		break;
	}

	default:
		vassert(false, "Invalid op: " + inst.getOpName(), getPCSigned());
	}

	vassert(out.rID != rZR, "[execute] Attempted write to zero register!", getPCSigned());

	return true;
}

std::string ExecuteStage::toString() const {
	return getValue().inst.toString();
}

bool WriteStage::execute(int &out) const {
	WBD wbd = this->wbd;

	if (wbd.rID != -1) {
		vassert(wbd.rID != rZR,
				"[writeback] Attempted write to zero register!", getPCSigned());
		pipeline->setR(wbd.rID, wbd.rV);
	}

	return true;
}

std::string WriteStage::toString() const {
	return wbd.toString();
}

std::string WBD::toString() const {
	std::ostringstream os;
	if (rID == -1) {
		os << ".";
	} else {
		os << "r" << std::dec << rID << " := "
		   << std::hex << rV;
	}
	return os.str();
}

bool shouldTakeBranch(Inst inst, u64 cond) {
	switch (inst.a) {
	case 0:
		return true;
	case 1: // Z
		return cond == 0;
	case 2: // NZ
		return cond != 0;
	case 3: // P
		return cond == 1;
	case 4: // N
		return cond == 2;
	default:
		vassert(false, "Unknown branch condition", -1);
	}
}
