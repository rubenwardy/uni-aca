#pragma once

#include "../VM.hpp"
#include "../ICPU.hpp"
#include "../../isa/isa.hpp"

using ISA::Inst;


class SimpleCPU : public ICPU {
	int counter = 0;
public:
	explicit SimpleCPU(VM::VM *vm):
		ICPU(vm)
	{}

	void step() override;
	void clear() override;
	std::string getCPUName() const override { return "Simple"; }

private:
	Inst fetch();
	bool execute(const Inst &inst);
};
