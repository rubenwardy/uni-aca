#include <iostream>

#include "SimpleCPU.hpp"

using namespace ISA;

void SimpleCPU::step() {
	auto &file = vm->getRegisterFile();

	Inst inst = fetch();
	file.set(rPC, file.get(rPC) + 1);

	fprintf(stderr, "Executing %x with %x %x %x\n", (int)inst.op, inst.a, inst.b, inst.imm);

	if (execute(inst)) {
		vm->notifyInstComplete();
	} else {
		file.set(rPC, file.get(rPC) - 1);
	}
}

void SimpleCPU::clear() {

}

Inst SimpleCPU::fetch() {
	auto &instructions = vm->getInstructions();

	u32 inst = instructions[vm->getRegisterFile().get(rPC)];
	return Inst(inst);
}

bool SimpleCPU::execute(const Inst &inst) {
	auto &file = vm->getRegisterFile();

	switch (inst.op) {
	case NOP:
		break;

	case ADD:
		file.set(inst.a, file.get(inst.b) + file.get(inst.c));
		break;

	case ADDI:
		file.set(inst.a, file.get(inst.b) + inst.getIMM64());
		break;

	case CMP: {
		u64 a = file.get(inst.a);
		u64 b = file.get(inst.b);
		if (a == b) {
			file.set(rCOND, 0);
		} else if (a > b) {
			file.set(rCOND, 1);
		} else if (a < b) {
			file.set(rCOND, 2);
		}
		break;
	}

	case LDR: {
		if (counter < LOAD_CYCLES) {
			counter++;
			return false;
		} else {
			counter = 0;

			u64 address = file.get(inst.b) + inst.imm;
			file.set(inst.a, vm->readMemory(address));
		}
		break;
	}

	case STR: {
		if (counter < STORE_CYCLES) {
			counter++;
			return false;
		} else {
			counter = 0;

			u64 address = file.get(inst.b) + inst.imm;
			vm->writeMemory(address, file.get(inst.a));
		}
		break;
	}

	case MUL: {
		file.set(inst.a, file.get(inst.b) * file.get(inst.c));
		break;
	}

	case DIV: {
		file.set(inst.a, file.get(inst.b) / file.get(inst.c));
		break;
	}

	case B: {
		bool do_branch = false;

		switch (inst.a) {
			case 0:
				do_branch = true;
				break;
			case 1: // Z
				do_branch = file.get(rCOND) == 0;
				break;
			case 2: // NZ
				do_branch = file.get(rCOND) != 0;
				break;
			case 3: // P
				do_branch = file.get(rCOND) == 1;
				break;
			case 4: // N
				do_branch = file.get(rCOND) == 2;
				break;
			default:
				fprintf(stderr, "Err %x\n", inst.a);
				break;
		}

		if (do_branch) {
			u64 dest = file.get(inst.b) + inst.getIMM64();
			std::cerr << "BRANCHING!" << std::endl;
			file.set(rPC, dest);
		}
		break;
	}

	default:
		vassert(false, "Invalid op: " + inst.getOpName(), (int)file.get(rPC) - 1);
	}

	return true;
}
