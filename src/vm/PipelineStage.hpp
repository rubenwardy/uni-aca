#pragma once

#include "../utils.hpp"


class IPipelineAPI {
public:
	virtual u32 getInstruction(u64 address) const = 0;
	virtual u64 readMemory(u64 address) = 0;
	virtual void writeMemory(u64 address, u64 value) = 0;
	virtual u64 getR(int id) = 0;
	virtual void setR(int id, u64 val) = 0;
};


template <class A, class B>
class PipelineStage {
protected:
	IPipelineAPI *pipeline;
	u64 pc = (u64)-1; // track PC for debugging
public:
	explicit PipelineStage(IPipelineAPI *pipeline):
		pipeline(pipeline)
	{}

	void setPC      (u64 address)   { pc = address; }
	u64  getPC      ()        const { return pc; }
	int  getPCSigned()        const { return (pc == (u64)-1) ? -1 : (int)pc; }
	virtual void setValue   (A value)       = 0;
	virtual A    getValue   ()        const = 0;
	virtual void resetValue ()              = 0;
	virtual bool execute    (B &out)  const = 0;
	virtual std::string toString() const = 0;
};

template <class A, class B, class C>
static bool InterlockStep(const PipelineStage<A, B> *before,
						  PipelineStage<B, C> *after, B *out = NULL) {
	B out_o;
	bool status = before->execute(out ? *out : out_o);

	if (after) {
		if (status) {
			after->setValue(out ? *out : out_o);
			after->setPC(before->getPC());
		} else {
			after->resetValue();
		}
	}

	return status;
}

