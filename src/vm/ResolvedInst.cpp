#include <sstream>
#include <iostream>
#include "ResolvedInst.hpp"

std::string ResolvedInst::toString() const {
	std::ostringstream ss;
	ss << "[" << seq;
	if (isSpeculative()) {
		ss << "|" << spec_parent;
	}
	ss << "] " << inst.toString() << "  |  ";

	auto read_rIDs = inst.getReadRegisters();
	if (read_rIDs[0] > -1) {
		if (read1val.isResolved()) {
			ss << std::hex << (u64)read1val;
		} else {
			ss << "?";
		}
	} else {
		ss << "-";
	}

	ss << " ";
	if (read_rIDs[1] > -1) {
		if (read2val.isResolved()) {
			ss << std::hex << (u64)read2val;
		} else {
			ss << "?";
		}
	} else {
		ss << "-";
	}

	return ss.str();
}

void ResolvedInst::attemptSpecCommit(u32 branch_seq) const {
	if (spec_parent == branch_seq) {
		std::cerr << "- " << toString() << " marked as non-spec" << std::endl;
		spec_parent = (u32)-1;
	}
}
