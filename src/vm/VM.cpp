#include "VM.hpp"
#include <iostream>

using namespace VM;
using namespace ISA;

::VM::VM::VM(FeatureFlags flags) :
		flags(flags)
{
	std::cerr << "VM created with flags:" << std::endl;
	std::cerr << " - pipelining: " << (flags.pipelining ? "on" : "off") << std::endl;
	std::cerr << " - super-scalar: " << (flags.super ? "on" : "off") << std::endl;

	cpu = ICPU::create(this);
	vassert(cpu, "Unable to init CPU", -1);

	std::cerr << "Initialized CPU: " << cpu->getCPUName() << std::endl;
}

bool ::VM::VM::step() {
	vassert(stats.cycles <= 3000,
			"Timed out, more than 100000 cycles reached", (int)file.getRaw(rPC));


	stats.cycles++;
	file.resetStats();
	memory.resetStats();

	if (!isRunnable()) {
		return false;
	}

	in_step = true;

	cpu->step();

	in_step = false;
	return true;
}

void ::VM::VM::reset() {
	memory.setup(instructions);
	cpu->clear();

	memset(&file,  0, sizeof(RegisterFile));
	memset(&stats, 0, sizeof(Stats));
}

void ::VM::VM::setInstructions(std::vector<u32> insts) {
	instructions = std::move(insts);
	memory.setup(instructions);
}

void ::VM::VM::printDebug() const {
	// %n FTW!
	int input;
	fprintf(stderr, "%d: %n", stats.cycles, &input);

	if (in_step) {
		fprintf(stderr, "[IN]");
	}

	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			int i = y + x*4;
			bool modified, read;
			u64 val = file.getRaw(i, modified, read);
			if (modified) {
				fprintf(stderr, COLOR_BLUE);
			} else if (read) {
				fprintf(stderr, COLOR_ORANGE);
			}
			fprintf(stderr, "%.2x=%.8lx ", i, val);
			fprintf(stderr, COLOR_CLEAR);
		}

		fprintf(stderr, "\n%*.s", input, " ");
	}
	{
		bool modified, read;
		u64 val = file.getRaw(16, modified, read);
		if (modified) {
			fprintf(stderr, COLOR_BLUE);
		} else if (read) {
			fprintf(stderr, COLOR_ORANGE);
		}
		fprintf(stderr, "%.2x=%.8lx\n", 16, val);
		fprintf(stderr, COLOR_CLEAR);
	}

	fprintf(stderr, "\n");
}

void ::VM::VM::printDump() const {
	for (int i = 0; i < 17; i++) {
		bool modified, read;
		u64 val = file.getRaw(i, modified, read);
		std::cout << std::dec << "r" << i << " = " << std::hex << val << std::endl;
	}

	std::cout << "mSize = " << std::dec << memory.size() << std::endl;

	for (int i = 0; i < memory.size(); i++) {
		std::cout << std::dec << "m" << i*2 << " = "
				<< std::hex << memory.getRaw(i) << std::endl;
	}

	std::cout << "ticks = " << std::dec << stats.cycles << std::endl;
	std::cout << "insts = " << std::dec << stats.insts << std::endl;
	std::cout << "branch_hit = " << std::dec << stats.branch_hit << std::endl;
	std::cout << "branch_miss = " << std::dec << stats.branch_miss << std::endl;
	std::cout << "ipc = " << std::dec
			<< (int)(100.f*((float)stats.insts / (float)(stats.cycles - 1))) << std::endl;

	if (stats.branch_hit + stats.branch_miss > 0) {
		std::cout << "branch_perc = " << std::dec
				  << (int)(100.f*((float)stats.branch_hit / (float)(stats.branch_miss + stats.branch_hit))) << std::endl;
	} else {
		std::cout << "branch_perc = 0" << std::endl;
	}
}

void ::VM::VM::writeMemory(u64 address, u64 value) {
	vassert(address >= 0 && address < 1000000/8,
			"Memory written out of bounds (1)", file.getRaw(rPC));
	vassert(address % 2 == 0, "Address not multiple of 2!", file.getRaw(rPC));

	address /= 2;

	if (address >= memory.size()) {
		vassert(expand_memory, "Memory written out of bounds (2)", (int)file.getRaw(rPC));
		memory.expandTo(address);
	}

	memory.write(address, value);
}

void ::VM::VM::notifyBranch(bool wasPredictionCorrect) {
	if (wasPredictionCorrect) {
		stats.branch_hit++;
	} else {
		stats.branch_miss++;
	}
}

u64 ::VM::VM::readMemory(u64 address) {
	vassert(address >= 0 && address < 1000000/8,
			"Memory written out of bounds (1)", file.getRaw(rPC));
	vassert(address % 2 == 0, "Address not multiple of 2!", file.getRaw(rPC));

	address /= 2;

	return (address >= memory.size()) ? 0 : memory.read(address);
}
