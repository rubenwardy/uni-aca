#include <utility>

#pragma once

#include "../utils.hpp"
#include "../isa/isa.hpp"
#include "types.hpp"
#include "ICPU.hpp"

#include <vector>
#include <string.h>

using ISA::rPC;
using ISA::Inst;

namespace VM {

struct Stats {
	int cycles = 0;
	int insts = 0;
	int branch_hit = 0;
	int branch_miss = 0;
};

class VM {
protected:
	RegisterFile file;
	Memory memory;

	std::vector<u32> instructions;

	const FeatureFlags flags;
	Stats stats;

	bool in_step = false;
	ICPU *cpu = nullptr;

	bool expand_memory = false;

public:
	explicit VM(FeatureFlags flags);

	void setInstructions(std::vector<u32> insts);
	void setPC(u64 pc) { file.set(rPC, pc); }
	bool step();
	void reset();

	void printDebug() const;
	void printDump() const;
	void notifyInstComplete() { stats.insts++; }

	const ICPU             *peekCPU()          const { return cpu;           }
	const std::vector<u32> &peekInstructions() const { return instructions;  }
	const Memory           &peekMemory()       const { return memory; }
	const RegisterFile     &peekRegisterFile() const { return file;          }
	const FeatureFlags     &peekFlags()        const { return flags;         }
	const Stats            &peekStats()        const { return stats;         }

	std::vector<u32>       &getInstructions()        { return instructions;  }
	RegisterFile           &getRegisterFile()        { return file;          }

	bool isRunnable() const { return cpu->isRunnable(); }

	void writeMemory(u64 address, u64 value);
	u64 readMemory(u64 address);

	void notifyBranch(bool wasPredictionCorrect);
};

};
