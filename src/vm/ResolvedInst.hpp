#pragma once

#include "../isa/isa.hpp"
#include "../utils.hpp"
#include "types.hpp"

using ISA::Inst;

class VirtualRegister {
	u64 v = 0;
	bool has_value = false;
public:
	VirtualRegister() = default;
	VirtualRegister(u64 v):
		v(v), has_value(true) {

	}

	operator u64() const {
		vassert(has_value, "Unexpected unresolved operand access", -1);
		return v;
	}

	u64 getOrZero() const {
		return has_value ? v : 0;
	}

	bool isResolved() const {
		return has_value;
	}
};

struct ResolvedInst {
	Inst inst = Inst();
	u64 pc = (u64)-1;
	u32 seq = 0;
	mutable u32 spec_parent = (u32)-1;

	ResolvedInst() = default;
	ResolvedInst(Inst inst, u64 pc, u32 seq, u32 spec_parent=(u32)-1):
		inst(inst), pc(pc), seq(seq), spec_parent(spec_parent)
	{}

	VirtualRegister read1val, read2val;

	std::string toString() const;

	bool operator==(const ResolvedInst &other) const {
		return seq == other.seq;
	}

	bool operator<(const ResolvedInst &other) const {
		return seq < other.seq;
	}

	bool operator<=(const ResolvedInst &other) const {
		return seq <= other.seq;
	}

	bool operator>(const ResolvedInst &other) const {
		return seq > other.seq;
	}

	bool isSpeculative() const {
		return spec_parent != (u32)-1;
	}

	bool isResolved() const {
		auto read_rIDs = inst.getReadRegisters();
		return (read_rIDs[0] == -1 || read1val.isResolved()) &&
				(read_rIDs[1] == -1 || read2val.isResolved());
	}

	void attemptSpecCommit(u32 branch_seq) const;
};
