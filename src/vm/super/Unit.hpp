#pragma once

#include "../../isa/isa.hpp"
#include "../types.hpp"
#include "../ResolvedInst.hpp"

using ISA::Inst;

enum ProcessorUnit {
	ARITHMETIC = 0,
	BRANCH = 1,
	LOAD_STORE = 2,
};



class SuperCPU;
class Unit {
protected:
	SuperCPU *cpu;
	ResolvedInst resolvedInst;

public:
	explicit Unit(SuperCPU *cpu):
		cpu(cpu)
	{}

	void push() { return push({ Inst(), (u64)-1, (u32)-1 }); }
	virtual void push(ResolvedInst inst) = 0;
	virtual bool canPush() const = 0;
	virtual void step() = 0;
	const ResolvedInst &getValue() const { return resolvedInst; }
	bool empty() const { return resolvedInst.pc == (u64)-1; }

	virtual ProcessorUnit getType() const = 0;

	static ProcessorUnit GetUnit(ISA::Op op) {
		switch (op) {
		case ISA::NOP:
		case ISA::ADD:
		case ISA::ADDI:
		case ISA::CMP:
		case ISA::MUL:
		case ISA::DIV:
			return ARITHMETIC;

		case ISA::LDR:
		case ISA::STR:
			return LOAD_STORE;

		case ISA::B:
			return BRANCH;

		default:
			vassert(false, "Unsupported opcode", -1);
		}
	}

};
