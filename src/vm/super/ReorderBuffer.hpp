#pragma once

#include <vector>
#include <map>
#include "../stages/stages.hpp"
#include "../ResolvedInst.hpp"

struct FinishedInstruction {
	ResolvedInst inst;
	WBD wbd;

	bool operator==(const FinishedInstruction &other) const {
		return inst == other.inst;
	}

	bool operator<(const FinishedInstruction &other) const {
		return inst < other.inst;
	}

	bool operator<=(const FinishedInstruction &other) const {
		return inst <= other.inst;
	}

	bool operator>(const FinishedInstruction &other) const {
		return inst > other.inst;
	}
};


class SuperCPU;
class ReorderBuffer {
	int next_seq = 0;
	std::set<FinishedInstruction> buffer;
	SuperCPU *cpu;
	std::map<u32,u32> seq_update_map;
	std::map<u32,bool> speculation_result;

public:
	const std::set<FinishedInstruction> &peek() const { return buffer; }


	explicit ReorderBuffer(SuperCPU *cpu):
		cpu(cpu)
	{}

	void insert(const ResolvedInst &inst, const WBD &wbd);
	void insert(FinishedInstruction finst);
	void step();
	bool empty() const { buffer.empty(); }

	void notifySpeculativeHit(u32 branch_seq);
	void notifySpeculativeMiss(u32 branch_seq, u32 firstWrongSeq, u32 correctSeq);
};
