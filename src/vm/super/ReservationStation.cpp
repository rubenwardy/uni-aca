#include <iostream>
#include <cassert>
#include "ReservationStation.hpp"
#include "SuperCPU.hpp"
#include "../types.hpp"

void ReservationStation::markAvailable() {
	std::bitset<17> written_before = { 0 };
	std::bitset<17> read_before = { 0 };


	std::cerr << "EU Locks:";
	for (int i = 0; i < 17; i++) {
		if (write_in_eu[i] != 0) {
			std::cerr << " " << i;
		}
	}
	std::cerr << std::endl;

	std::cerr << "Reorder Locks:";
	for (int i = 0; i < 17; i++) {
		auto item = write_in_reorder[i];
		if (item != 0) {
			std::cerr << " " << i << "[" << item << "]";
		}
	}
	std::cerr << std::endl;

	std::cerr << "Mark Available:" << std::endl;


	for (auto &entry : entries) {
		std::cerr << "- " << entry.resolvedInst.seq << " (" << entry.resolvedInst.inst.toString() << ")" << std::endl;

		// Mark write unavailable if previous instructions require old value
		// Also mark unavailable if write instruction in flight
		signed char write_rID = entry.resolvedInst.inst.getWriteRegister();
		if (write_rID > -1) {
			bool read_lock = read_before[write_rID];
			bool writ_lock = write_in_eu[write_rID] || written_before[write_rID];

			entry.write_avail = !read_lock && !writ_lock;
		} else {
			entry.write_avail = true;
		}

		// Mark input unavailable if written to before, or written in flight
		auto read_rIDs = entry.resolvedInst.inst.getReadRegisters();
		for (int i = 0; i < 2; i++) {
			auto read_rID = read_rIDs[i];
			if (read_rID > -1) {
				read_before[read_rID] = true;
			}
		}

		// Set write
		if (write_rID > -1) {
			written_before[write_rID] = true;
		}


		std::cerr << "   -  W: " << (entry.write_avail ? "OK" : "BLOCKED") << std::endl;
		std::cerr << "   - R1: " << (entry.resolvedInst.read1val.isResolved() ? "OK" : "BLOCKED") << std::endl;
		std::cerr << "   - R2: " << (entry.resolvedInst.read2val.isResolved() ? "OK" : "BLOCKED") << std::endl;
	}
}

void ReservationStation::add(ResolvedInst inst) {
	std::cerr << "Adding instruction to reserv: "
			<< inst.inst.getOpName()
			<< " "
			<< inst.inst.getOperands()
			<< std::endl;

	vassert(entries.size() < MAX_SIZE, "Max size reached!", (int)inst.pc);

	//
	// Fetch operands
	//
	std::bitset<17> written_before = { 0 };
	for (const auto &entry : entries) {
		auto write_rID = entry.resolvedInst.inst.getWriteRegister();
		if (write_rID > -1) {
			written_before[write_rID] = true;
		}
	}


	auto read_rIDs = inst.inst.getReadRegisters();
	if (read_rIDs[0] > -1 && !isWriteLocked(read_rIDs[0]) && !written_before[read_rIDs[0]]) {
		inst.read1val = cpu->getR((ISA::Reg)read_rIDs[0]);
	}
	if (read_rIDs[1] > -1 && !isWriteLocked(read_rIDs[1]) && !written_before[read_rIDs[1]]) {
		inst.read2val = cpu->getR((ISA::Reg)read_rIDs[1]);
	}
	entries.emplace_back(inst);
}

bool ReservationStation::dispatch() {
	bool seen_str = false;
	for (const auto &entry : entries) {
		auto unitType = Unit::GetUnit(entry.resolvedInst.inst.op);
		if (unitType == LOAD_STORE && seen_str) {
			continue;
		}

		if (entry.resolvedInst.inst.op == ISA::STR) {
			seen_str = true;
		}

		if (!entry.isAvail()) {
			continue;
		}

		if (entry.resolvedInst.isSpeculative() &&
				entry.resolvedInst.inst.op == ISA::STR) {
			continue;
		}

		Unit *unit = cpu->getFreeUnit(unitType);
		if (!unit) {
			continue;
		}

		vassert(unit->canPush(),
				"getFreeUnit() returned a unit with canPush false!", entry.resolvedInst.pc);

		dispatch(entry, unit);
		return true;
	}
	return false;
}

void ReservationStation::dispatch(const RSEntry &entry, Unit *unit) {
	ResolvedInst resolvedInst = entry.resolvedInst;

	std::cerr << "[RS] Dispatching " << resolvedInst.toString() << std::endl;

	//
	// Block registers
	//
	signed char write_rID = resolvedInst.inst.getWriteRegister();
	if (write_rID > -1) {
		assert(!write_in_eu[write_rID]);
		write_in_eu[write_rID] = true;
	}

	//
	// Dispatch
	//
	entries.remove(entry);
	unit->push(resolvedInst);
}

void ReservationStation::notifyInstComplete(const ResolvedInst &inst, const WBD &wbd) {
	vassert(inst.pc != (u64)-1,
			"Received invalid instruction to ReservationStation.notifyInstComplete()", (int)inst.pc);

	if (wbd.rID > -1) {
		write_in_eu[wbd.rID] = false;

		if (inst.isSpeculative()) {
			auto spec = speculation_result.find(inst.spec_parent);
			if (spec != speculation_result.end() && !spec->second) {
				// If a spec op was holding a write lock, make sure to release
				// and dependants
				if (write_in_reorder[wbd.rID] == 0) {
					for (auto &entry : entries) {
						auto read_rIDs = entry.resolvedInst.inst.getReadRegisters();
						if (read_rIDs[0] == wbd.rID) {
							entry.resolvedInst.read1val = cpu->getR((ISA::Reg)wbd.rID);
						} else if (read_rIDs[1] == wbd.rID) {
							entry.resolvedInst.read2val = cpu->getR((ISA::Reg)wbd.rID);
						}

						if (entry.resolvedInst.inst.getWriteRegister() == wbd.rID) {
							break;
						}
					}
				}

				return;
			}
		}

		write_in_reorder[wbd.rID]++;

		for (auto &entry : entries) {
			auto read_rIDs = entry.resolvedInst.inst.getReadRegisters();
			if (read_rIDs[0] == wbd.rID) {
				entry.resolvedInst.read1val = wbd.rV;
			} else if (read_rIDs[1] == wbd.rID) {
				entry.resolvedInst.read2val = wbd.rV;
			}

			if (entry.resolvedInst.inst.getWriteRegister() == wbd.rID) {
				break;
			}
		}
	}

	markAvailable();
}

void ReservationStation::notifyWriteback(const ResolvedInst &inst, const WBD &wbd) {
	vassert(inst.pc != (u64)-1, "Received invalid instruction to ReservationStation.notifyInstComplete()", (int)inst.pc);


	// TODO: mark as available won't take write_in_reorder into account

	if (wbd.rID > -1) {
		write_in_reorder[wbd.rID]--;
		assert(write_in_reorder[wbd.rID] >= 0);

		if (!isWriteLocked(wbd.rID)) {
			for (auto &entry : entries) {
				auto read_rIDs = entry.resolvedInst.inst.getReadRegisters();
				if (read_rIDs[0] == wbd.rID) {
					entry.resolvedInst.read1val = wbd.rV;
				} else if (read_rIDs[1] == wbd.rID) {
					entry.resolvedInst.read2val = wbd.rV;
				}

				if (entry.resolvedInst.inst.getWriteRegister() == wbd.rID) {
					break;
				}
			}
		}
	}

	markAvailable();
}

void ReservationStation::notifySpeculativeHit(u32 branch_seq) {
	speculation_result[branch_seq] = true;
	std::cerr << "[RS] Speculative hit at " << std::dec << branch_seq <<  std::endl;
	for (auto &entry : entries) {
		entry.resolvedInst.attemptSpecCommit(branch_seq);
	}
}

void ReservationStation::notifySpeculativeMiss(u32 branch_seq) {
	speculation_result[branch_seq] = false;

	std::cerr << "[RS] Speculative miss at " << std::dec << branch_seq <<  std::endl;

	for (auto it = entries.begin(); it != entries.end();) {
		if (it->resolvedInst.spec_parent == branch_seq) {
			std::cerr << "- " << it->resolvedInst.toString() << " erased" << std::endl;
			entries.erase(it++);
		} else {
			std::cerr << "- " << it->resolvedInst.toString() << " ok" << std::endl;
			it++;
		}
	}
}

void ReservationStation::notifyReorderDrop(const ResolvedInst &inst, const WBD &wbd) {
	if (wbd.rID != -1) {
		write_in_reorder[wbd.rID]--;
		assert(write_in_reorder[wbd.rID] >= 0);
	}
}

bool ReservationStation::allLocksFree() const {
	for (auto item : write_in_reorder) {
		if (item != 0) {
			return false;
		}
	}

	for (int i = 0; i < 17; i++) {
		if (write_in_eu[i] != 0) {
			return false;
		}
	}

	return true;
}

