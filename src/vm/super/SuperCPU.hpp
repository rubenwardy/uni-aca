#pragma once

#include "ReservationStation.hpp"

#include "../VM.hpp"
#include "../ICPU.hpp"
#include "ArithmeticUnit.hpp"
#include "BranchUnit.hpp"
#include "LoadStoreUnit.hpp"
#include "ReorderBuffer.hpp"

class SuperCPU : public ICPU {
	ReservationStation reservationStation = ReservationStation(this);
	ReorderBuffer reorderBuffer = ReorderBuffer(this);

	ArithmeticUnit alu1 = ArithmeticUnit(this);
	ArithmeticUnit alu2 = ArithmeticUnit(this);
	BranchUnit bu = BranchUnit(this);
	LoadStoreUnit lsu = LoadStoreUnit(this);
	std::vector<Unit*> units;

	u32 seq_counter = 0;
	u32 branch_seq = (u32)-1;
	bool guessed_taken = false;
	bool branch_halted = false;
	u64 pc = 0;

	std::map<u64, bool> branch_cache;

public:
	explicit SuperCPU(VM::VM *vm);

	void step() override;
	void clear() override;
	bool isRunnable() const override;
	bool hasMarks() const override { return true; }
	std::vector<Mark> getMarks() const override;
	std::string getCPUName() const override { return "Super"; }
	Unit *getFreeUnit(ProcessorUnit type);

	/// Instruction has been completed by an EU
	///
	/// * Alerts RS of completion and of written values.
	/// * Adds instruction to re-order buffer.
	/// * This will be called during an EU's update step.
	void notifyInstComplete(const ResolvedInst &inst, const WBD &wbd);

	/// Reorder buffer has committed an instruction, write it to the registerfile
	void notifyWriteback(const ResolvedInst &inst, const WBD &wbd);

	/// A branch has completed
	/// If guess was wrong:
	///  * Clear speculatively executed instructions in decode queue.
	///  * Go to correct PC, and issue from there.
	/// If guess was correct, just clear the speculative info.
	/// The fetch will continue into more branches if it had halted.
	void notifyBranch(const ResolvedInst &inst, bool taken, u64 target);

	inline u64 getR(ISA::Reg reg) {
		return vm->getRegisterFile().get(reg);
	}

	u64 readMemory(u64 address) {
		return vm->readMemory(address);
	}

	void writeMemory(u64 address, u64 value) {
		vm->writeMemory(address, value);
	}


	void notifyReorderDrop(const ResolvedInst &inst, const WBD &wbd);

private:
	std::vector<ResolvedInst> fetch_buffer;

	inline void setR(ISA::Reg reg, u64 value) {
		vm->getRegisterFile().set(reg, value);
	}

	inline void addR(ISA::Reg reg, u64 v) {
		setR(reg, getR(reg) + v);
	}

	bool isSpeculating() const {
		return branch_seq != (u32)-1;
	}

	size_t calcFetchSize(size_t offset=0) const;
	void fetch();
	void decode();
	void dispatch();
	void unitStep();
};
