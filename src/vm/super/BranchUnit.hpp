#pragma once

#include "Unit.hpp"

class BranchUnit : public Unit {
	bool _canPush = true;
public:
	explicit BranchUnit(SuperCPU *cpu):
		Unit(cpu)
	{}

	void push(ResolvedInst inst) override;
	bool canPush() const override { return _canPush; }
	void step() override;
	ProcessorUnit getType() const override { return ProcessorUnit::BRANCH; }
};

