#include <iostream>
#include "../stages/stages.hpp"
#include "BranchUnit.hpp"
#include "SuperCPU.hpp"

void BranchUnit::push(ResolvedInst inst) {
	vassert(inst.inst.op == ISA::B,
			"Attempt to push non-branch instruction to BU", (int)inst.pc);
	resolvedInst = inst;
	_canPush = false;
}


void BranchUnit::step() {
	if (empty()) {
		return;
	}

	bool taken = shouldTakeBranch(resolvedInst.inst, resolvedInst.read1val.getOrZero());
	u64 address = resolvedInst.read2val + resolvedInst.inst.getIMM64();

	WBD wbd;
	if (taken) {
		std::cerr << "BRANCHING!" << std::endl;
	} else {
		std::cerr << "NOT BRANCHING!" << std::endl;
	}

	cpu->notifyInstComplete(resolvedInst, wbd);
	cpu->notifyBranch(resolvedInst, taken, address);
	resolvedInst = ResolvedInst();
	_canPush = true;
}
