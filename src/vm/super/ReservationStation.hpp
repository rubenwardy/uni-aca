#pragma once

#include <bitset>
#include <list>
#include <src/vm/stages/stages.hpp>
#include <map>

#include "../../utils.hpp"
#include "../../isa/isa.hpp"
#include "../ResolvedInst.hpp"
#include "Unit.hpp"


using ISA::Inst;

struct RSEntry {
	explicit RSEntry(ResolvedInst inst):
		resolvedInst(inst)
	{}

	ResolvedInst resolvedInst;

	/// True if dependent on previous instructions, in pipeline or in station
	bool write_avail = false;//, read1_avail = false, read2_avail = false;

	bool isAvail() const {
		return write_avail && resolvedInst.isResolved();// && read1_avail && read2_avail;
	}

	bool operator==(const RSEntry &other) const {
		return resolvedInst == other.resolvedInst;
	}
};

class SuperCPU;
class ReservationStation {
	std::list<RSEntry> entries;
	SuperCPU *cpu;

	/// Is there an instructions in flight *writing* to register
	std::bitset<17> write_in_eu = { 0 };
	std::array<int, 17> write_in_reorder = { 0 };
	std::map<u32, bool> speculation_result;

	bool isWriteLocked(int reg) const {
		return write_in_eu[reg] || write_in_reorder[reg] > 0;
	}

public:
	explicit ReservationStation(SuperCPU *cpu):
		cpu(cpu)
	{}

	/// Identifies instructions to dispatch
	bool dispatch();

	/// Dispatches an instruction to a unit
	void dispatch(const RSEntry &entry, Unit *unit);

	/// Sets avail flags in entry log
	///
	/// Starts at the front most instruction and scans up.
	/// Keeps track of RAW.
	void markAvailable();

	/// Returns number of entries in station
	size_t size() const { return entries.size(); }

	/// Insert an instruction into the station
	/// Will through an exception if the station is full
	void add(ResolvedInst inst);

	/// Retrieve readonly reference to entry list
	const std::list<RSEntry> &peekEntries() const { return entries; }

	/// Maximum number of itesm
	static const size_t MAX_SIZE = 20, MAX_DISPATCH_PER_CYCLE=4;

	/// Frees EU register locks, writes value to any pending instructions
	void notifyInstComplete(const ResolvedInst &inst, const WBD &wbd);

	/// Frees reorder register locks, writes value to any pending instructions
	void notifyWriteback(const ResolvedInst &inst, const WBD &wbd);

	bool empty() const { return entries.empty(); }

	void notifySpeculativeHit(u32 branch_seq);

	void notifySpeculativeMiss(u32 branch_seq);

	void notifyReorderDrop(const ResolvedInst &inst, const WBD &wbd);

	bool allLocksFree() const;
};


