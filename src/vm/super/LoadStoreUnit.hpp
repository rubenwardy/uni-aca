#pragma once

#include "Unit.hpp"

class LoadStoreUnit : public Unit {
	bool _canPush = true;
	int retries = 0;
public:
	explicit LoadStoreUnit(SuperCPU *cpu):
		Unit(cpu)
	{}

	void push(ResolvedInst inst) override;
	bool canPush() const override { return _canPush; }
	void step() override;
	ProcessorUnit getType() const override { return ProcessorUnit::LOAD_STORE; }
};
