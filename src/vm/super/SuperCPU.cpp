#include <iostream>
#include <assert.h>
#include "SuperCPU.hpp"

void SuperCPU::step() {
	reorderBuffer.step();
	unitStep();
	dispatch();
	decode();
	fetch();

	setR(rPC, pc);
}

void SuperCPU::clear() {
	pc = 0;
	branch_cache.clear();
	fetch_buffer.clear();
	reservationStation = ReservationStation(this);
	reorderBuffer      = ReorderBuffer(this);
	alu1 = ArithmeticUnit(this);
	alu2 = ArithmeticUnit(this);
	lsu = LoadStoreUnit(this);
	bu  = BranchUnit(this);
	seq_counter = 0;
}

bool SuperCPU::isRunnable() const {
	bool runnable = calcFetchSize() > 0 || !fetch_buffer.empty() || !reservationStation.empty() ||
			!reorderBuffer.empty() || !alu1.empty() || !alu2.empty() || !bu.empty() || !lsu.empty();

	if (!runnable) {
		vassert(reservationStation.allLocksFree(), "A lock is held!", -1);
	}

	return runnable;
}

std::vector<ICPU::Mark> SuperCPU::getMarks() const {
	std::vector<Mark> marks;

	for (size_t i = 0; i < calcFetchSize(fetch_buffer.size()); i++) {
		marks.emplace_back("Fetch", pc + i,
						   std::to_string(pc + 1), "#26326d");
	}

	for (const auto &item : fetch_buffer) {
		u64 pc = item.pc;
		marks.emplace_back("Decode", (int)((pc == (u64)-1) ? -1 : pc),
						   item.toString(), "#314062");
	}

	for (const auto &item : reservationStation.peekEntries()) {
		u64 pc = item.resolvedInst.pc;
		marks.emplace_back("Reserv", (int)((pc == (u64)-1) ? -1 : pc),
						   item.resolvedInst.toString(), "#3b4d56");
	}

	auto &alu1_val = alu1.getValue();
	if (alu1_val.pc != (u64)-1) {
		marks.emplace_back("ALU1",  alu1_val.pc, alu1_val.toString(), "#0082d4");
	}

	auto &alu2_val = alu2.getValue();
	if (alu2_val.pc != (u64)-1) {
		marks.emplace_back("ALU2",  alu2_val.pc, alu2_val.toString(), "#0082d4");
	}

	auto &bu_val = bu.getValue();
	if (bu_val.pc != (u64)-1) {
		marks.emplace_back("BU",  bu_val.pc, bu_val.toString(), "#d48200");
	}

	auto &lsu_val = lsu.getValue();
	if (lsu_val.pc != (u64)-1) {
		marks.emplace_back("LSU",  lsu_val.pc, lsu_val.toString(), "#00d482");
	}

	for (const auto &item : reorderBuffer.peek()) {
		marks.emplace_back("Reorder",  item.inst.pc, item.inst.toString(), "#610506");
	}

	return marks;
}

size_t SuperCPU::calcFetchSize(size_t offset) const {
	if (branch_halted) {
		// Stop fetch if waiting for halting branch
		return 0;
	}

	size_t to_add = ReservationStation::MAX_SIZE - reservationStation.size() - offset;
	int remaining = (int)vm->peekInstructions().size() - (int)pc;
	if (remaining < 0) {
		return 0;
	}

	return std::min<size_t>(std::min<size_t>(to_add, 4), (size_t)remaining);
}

void SuperCPU::fetch() {
	if (branch_halted) {
		return;
	}

	assert(fetch_buffer.empty());

	std::cerr << "[SuperCPU][fetch] Starting fetch with pc=" << pc << std::endl;

	bool speculativeEnabled = vm->peekFlags().speculative;

	int added = 0;
	while (added < 4 && calcFetchSize(added) > 0) {
		added++;
		u32 inst = vm->peekInstructions()[pc];

		ResolvedInst seqInst = ResolvedInst(Inst(inst), pc, seq_counter, branch_seq);



		if (seqInst.inst.op == ISA::B) {
			std::cerr << "[SuperCPU][fetch] - Handling branch:" << std::endl;
			if (isSpeculating()) {
				vassert(speculativeEnabled, "Speculation is not enabled!", pc);

				std::cerr << "[SuperCPU][fetch]   - already speculating, abort" << std::endl;
				// Is already speculating, don't issue
				break;
			} else if (seqInst.inst.b != ISA::rZR || !speculativeEnabled) {
				std::cerr << "[SuperCPU][fetch]   - can't speculate, halting prediction"
						  << std::endl;
				// Is non-const address, don't speculate
				branch_halted = true;
			} else if (seqInst.inst.a == 0) {
				vm->notifyBranch(true);
				vm->notifyInstComplete();
				pc = seqInst.inst.getIMM64();
				added--;
				continue;
			} else {
				vassert(speculativeEnabled, "Speculation is not enabled!", pc);

				branch_seq = seqInst.seq;
				guessed_taken = false;


				switch (vm->peekFlags().bp_type) {
				case VM::FeatureFlags::NEVER_TAKE:
					guessed_taken = false;
					break;

				case VM::FeatureFlags::ALWAYS_TAKE:
					guessed_taken = true;
					break;

				case VM::FeatureFlags::DYNAMIC: {
					auto last = branch_cache.find(pc);
					if (last != branch_cache.end()) {
						guessed_taken = last->second;
						break;
					}

					// fall through
				}

				case VM::FeatureFlags::STATIC:
					guessed_taken = seqInst.inst.imm < pc || pc == 0;
					break;

				default:
					vassert(false, "Unknown BP method", pc);
				}


				if (guessed_taken) {
					pc = seqInst.inst.getIMM64();
					std::cerr << "[SuperCPU][fetch]   - speculating taken" << std::endl;
				} else {
					pc++;
					std::cerr << "[SuperCPU][fetch]   - speculating not taken" << std::endl;
				}


				fetch_buffer.emplace_back(seqInst);
				seq_counter++;
				continue;
			}
		}

		fetch_buffer.emplace_back(seqInst);
		seq_counter++;
		pc++;
	}
}

void SuperCPU::decode() {
	for (auto item : fetch_buffer) {
		reservationStation.add(item);
	}

	reservationStation.markAvailable();
	fetch_buffer.clear();
}

void SuperCPU::dispatch() {
	int dispatches = 0;
	while (dispatches < ReservationStation::MAX_DISPATCH_PER_CYCLE &&
			reservationStation.dispatch()) {
		reservationStation.markAvailable();
		dispatches++;
	}
}

Unit *SuperCPU::getFreeUnit(ProcessorUnit type) {
	for (auto unit : units) {
		if (unit->getType() == type && unit->canPush()) {
			return unit;
		}
	}
	return nullptr;
}

void SuperCPU::notifyInstComplete(const ResolvedInst &inst, const WBD &wbd) {
	if (inst.pc == (u64)-1) {
		return;
	}

	vassert(wbd.rID != ISA::rZR, "Attempt to write to ZR", (int)inst.pc);

	std::cerr << "[CPU][notifyInstComplete] Inst complete" << inst.toString() << std::endl;
	reorderBuffer.insert(inst, wbd);
	reservationStation.notifyInstComplete(inst, wbd);
}

void SuperCPU::notifyWriteback(const ResolvedInst &inst, const WBD &wbd) {
	vassert(!inst.isSpeculative(), "Unexpected specular instruction writeback", inst.pc);

	if (wbd.rID != -1) {
		vassert(wbd.rID != ISA::rZR, "[CPU][writeback] Attempted write to zero register!", -1);
		vassert(wbd.rID != ISA::rPC, "[CPU][writeback] Attempted write to PC!", -1);

		std::cerr << "[CPU][writeback] r" << std::dec << wbd.rID << std::hex << wbd.rV << std::endl;
		setR((ISA::Reg)wbd.rID, wbd.rV);

		reservationStation.notifyWriteback(inst, wbd);
	}

	vm->notifyInstComplete();
}

void SuperCPU::notifyBranch(const ResolvedInst &inst, bool taken, u64 target) {
	std::cerr << "[SuperCPU] - got branch result" << std::endl;

	if (vm->peekFlags().speculative) {
		branch_cache[inst.pc] = taken;
	}

	if (isSpeculating()) {
		vassert(vm->peekFlags().speculative, "Speculation is not enabled!", pc);

		if (guessed_taken != taken) {
			std::cerr << "[SuperCPU]   - misprediction!!!" << std::endl;
			pc = taken ? target : (inst.pc + 1);

			fetch_buffer.clear();
			reservationStation.notifySpeculativeMiss(branch_seq);
			reorderBuffer.notifySpeculativeMiss(branch_seq, branch_seq + 1, seq_counter);
			vm->notifyBranch(false);
		} else {
			std::cerr << "[SuperCPU]   - correct prediction!!!" << std::endl;
			reservationStation.notifySpeculativeHit(branch_seq);
			reorderBuffer.notifySpeculativeHit(branch_seq);
			vm->notifyBranch(true);
		}
	} else {
		std::cerr << "[SuperCPU]   - halted type" << std::endl;
		pc = taken ? target : (inst.pc + 1);
		vm->notifyBranch(false);
	}


	branch_halted = false;
	branch_seq = (u32)-1;
}

void SuperCPU::unitStep() {
	alu1.step();
	alu2.step();
	lsu.step();
	bu.step();
}

void SuperCPU::notifyReorderDrop(const ResolvedInst &inst, const WBD &wbd) {
	reservationStation.notifyReorderDrop(inst, wbd);
}

SuperCPU::SuperCPU(VM::VM *vm) :
		ICPU(vm)
{
	units.push_back(&alu1);
	units.push_back(&alu2);
	units.push_back(&lsu);
	units.push_back(&bu);
}

