#include "ArithmeticUnit.hpp"
#include "SuperCPU.hpp"

void ArithmeticUnit::push(ResolvedInst inst) {
	vassert(inst.inst.op != ISA::B,
			"Attempt to push branch instruction to ALU", (int)inst.pc);

	resolvedInst = inst;
	_canPush = false;
}

bool ArithmeticUnit::canPush() const {
	return _canPush;
}

void ArithmeticUnit::step() {
	execute.setValue(resolvedInst);
	execute.setPC(resolvedInst.pc);

	WBD wbd;
	if (!execute.execute(wbd)) {
		return;
	}

	cpu->notifyInstComplete(resolvedInst, wbd);

	resolvedInst = ResolvedInst();
	execute.resetValue();
	_canPush = true;
}
