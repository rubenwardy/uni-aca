#pragma once

#include <src/vm/stages/stages.hpp>
#include "Unit.hpp"

class ArithmeticUnit : public Unit {
	ExecuteStage execute = ExecuteStage(nullptr);
	bool _canPush = true;

public:
	explicit ArithmeticUnit(SuperCPU *cpu):
		Unit(cpu)
	{}

	void push(ResolvedInst inst) override;
	bool canPush() const override;
	void step() override;
	ProcessorUnit getType() const override { return ProcessorUnit::ARITHMETIC; }
};
