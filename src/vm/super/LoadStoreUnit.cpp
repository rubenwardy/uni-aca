#include "LoadStoreUnit.hpp"
#include "SuperCPU.hpp"

void LoadStoreUnit::push(ResolvedInst inst) {
	vassert(inst.inst.op == ISA::LDR || inst.inst.op == ISA::STR,
			"Attempt to push non-loadstr instruction to LSU", (int)inst.pc);


	vassert(inst.inst.op != ISA::STR || !inst.isSpeculative(),
			"Attempt to push speculative store to LSU", (int)inst.pc);

	resolvedInst = inst;
	_canPush = false;
}

void LoadStoreUnit::step() {
	if (empty()) {
		return;
	}

	WBD wbd;
	u64 address = resolvedInst.read1val + resolvedInst.inst.getIMM64();
	if (resolvedInst.inst.op == ISA::LDR) {
		if (retries < ISA::LOAD_CYCLES) {
			retries++;
			return;
		}

		wbd.rID = resolvedInst.inst.a;
		wbd.rV  = cpu->readMemory(address);
	} else if (resolvedInst.inst.op == ISA::STR) {
		if (retries < ISA::STORE_CYCLES) {
			retries++;
			return;
		}

		cpu->writeMemory(address, resolvedInst.read2val);
	}

	cpu->notifyInstComplete(resolvedInst, wbd);
	resolvedInst = ResolvedInst();
	_canPush = true;
	retries = 0;
}
