#include <iostream>
#include "ReorderBuffer.hpp"
#include "SuperCPU.hpp"

void ReorderBuffer::insert(const ResolvedInst &inst, const WBD &wbd) {
	FinishedInstruction fi;
	fi.inst = inst;
	fi.wbd = wbd;
	insert(fi);
}

void ReorderBuffer::insert(FinishedInstruction finst) {
	vassert(finst.inst.pc != (u64)-1, "Received invalid instruction to reorder buffer", -1);

	if (finst.inst.isSpeculative()) {
		auto spec = speculation_result.find(finst.inst.spec_parent);
		if (spec != speculation_result.end()) {
			if (spec->second) {
				finst.inst.spec_parent = (u32)-1;
				std::cerr << finst.inst.toString() << " marked as non-speculative in reorder buffer" << std::endl;
			} else {
				std::cerr << finst.inst.toString() << " discarded on insert to reorder buffer" << std::endl;
				return;
			}
		}
	}

	vassert(finst.inst.seq >= next_seq,
			"Unexpected insertion of instruction already committed by reorder buffer", (int)finst.inst.pc);

	buffer.insert(finst);
}

void ReorderBuffer::step() {
	for (auto it = buffer.begin(); it != buffer.end();) {
		auto replacement = seq_update_map.find(next_seq);
		if (replacement != seq_update_map.end()) {
			std::cerr << "[reorder] Found replacement: " << std::dec << next_seq
					<< "->" << replacement->second << std::endl;
			next_seq = replacement->second;
		}

		vassert(it->inst.seq >= next_seq,
				"Unexpected occurrence of instruction already committed by reorder buffer",
					(int)it->inst.pc);

		if (it->inst.seq != next_seq) {
			std::cerr << it->inst.toString() << " is not next seq: " << next_seq << std::endl;
			break;
		}

		if (it->inst.isSpeculative()) {
			std::cerr << it->inst.toString() << " is speculative, don't commit yet" << std::endl;
			break;
		}

		std::cerr << "Seq " << it->inst.seq << " complete!" << std::endl;
		next_seq++;
		cpu->notifyWriteback(it->inst, it->wbd);
		buffer.erase(it++);
	}
}

void ReorderBuffer::notifySpeculativeHit(u32 branch_seq) {
	speculation_result[branch_seq] = true;

	std::cerr << "[Reorder] Speculative hit at " << std::dec << branch_seq <<  std::endl;

	for (auto &entry : buffer) {
		if (entry.inst.spec_parent == branch_seq) {
			entry.inst.spec_parent = (u32)-1;
		}
	}

//	for (auto it = buffer.begin(); it != buffer.end();) {
//		if (it->inst.spec_parent == branch_seq) {
//			FinishedInstruction entry = *it;
//			entry.inst.spec_parent = (u32)-1;
//
//			buffer.erase(it++);
//			buffer.insert(entry);
//
//			std::cerr << "- " << entry.inst.toString() << " marked as non-spec" << std::endl;
//		}
//	}

}

void ReorderBuffer::notifySpeculativeMiss(u32 branch_seq, u32 firstWrongSeq, u32 correctSeq) {
	seq_update_map[firstWrongSeq] = correctSeq;
	speculation_result[branch_seq] = false;

	std::cerr << "[Reorder] Speculative miss at " << std::dec << branch_seq <<  std::endl;

	// Erase speculative instructions
	for (auto it = buffer.begin(); it != buffer.end();) {
		if (it->inst.spec_parent == branch_seq) {
			std::cerr << "- " << it->inst.toString() << " erased" << std::endl;
			cpu->notifyReorderDrop(it->inst, it->wbd);
			buffer.erase(it++);
		} else {
			std::cerr << "- " << it->inst.toString() << " ok" << std::endl;
			it++;
		}
	}
}

