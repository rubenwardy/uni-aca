#include <sstream>
#include <iostream>
#include "types.hpp"

VM::VMException::VMException(std::string info, int pc):
	info(info),
	pc(pc)
{
	std::ostringstream os;
	os << info;
	if (pc != -1) {
		os << " at pc=" << std::hex << pc;
	}
	os << std::endl;
	msg = os.str();
}
