#include "ICPU.hpp"
#include "simple/SimpleCPU.hpp"
#include "pipelined/PipelinedCPU.hpp"
#include "super/SuperCPU.hpp"


ICPU *ICPU::create(VM::VM *vm) {
	auto &flags = vm->peekFlags();

	if (flags.pipelining) {
		if (flags.super) {
			return new SuperCPU(vm);
		} else {
			return new VM::PipelinedCPU(vm);
		}
	} else {
		return new SimpleCPU(vm);
	}
}

bool ICPU::isRunnable() const {
	return vm->peekRegisterFile().getRaw(rPC) < vm->peekInstructions().size();
}
