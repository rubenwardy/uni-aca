#pragma once

#include "../utils.hpp"
#include <utility>
#include <string.h>
#include <vector>
#include <set>
#include <src/isa/isa.hpp>

namespace VM {

// struct WritebackData {
// 	short register;
// 	u64 value;
// };

class VMException : public std::exception {
public:
	std::string info;
	int pc;
	std::string msg;

	VMException(std::string info, int pc);

	const char *what() const noexcept override { return msg.c_str(); }
};

#define vassert(test, message, pc)                                             \
	if (!(test)) {                                                             \
		throw ::VM::VMException(std::string(__FILE__) + ":" +  std::to_string(__LINE__) + " " + (message), (int)(pc)); \
	}


class RegisterFile {
	u64 regs[17] = { 0 };
public:
	bool modified[17] = { false };
	bool read[17] = { false };

	u64 get(int reg) {
		vassert(reg >= 0 && reg <= 16, "Register ID out of bounds", -1);
		read[reg] = true;
		return regs[reg];
	}

	u64 getRaw(int reg) const {
		vassert(reg >= 0 && reg <= 16, "Register ID out of bounds", -1);
		return regs[reg];
	}

	u64 getRaw(int reg, bool &w, bool &r) const {
		vassert(reg >= 0 && reg <= 16, "Register ID out of bounds", -1);
		w = modified[reg];
		r = read[reg];
		return regs[reg];
	}

	void set(int reg, u64 val) {
		vassert(reg >= 0 && reg <= 16, "Register ID out of bounds", -1);
		modified[reg] = true;
		regs[reg] = val;
	}

	void setRaw(int reg, u64 val) {
		vassert(reg >= 0 && reg <= 16, "Register ID out of bounds", -1);
		regs[reg] = val;
	}

	void resetStats() {
		memset(modified, 0, sizeof(modified));
		memset(read,     0, sizeof(read));
	}
};

class Memory {
	std::vector<u64> memory;

public:
	std::set<u64> modified_words;
	std::set<u64> read_words;

	void write(u64 address, u64 value) {
		modified_words.insert(address);
		memory[address] = value;
	}

	u64 read(u64 address) {
		read_words.insert(address);
		return memory[address];
	}

	u64 getRaw(u64 address) const {
		return memory[address];
	}

	u64 size() const {
		return memory.size();
	}

	const std::vector<u64> &peek() const {
		return memory;
	}

	void expandTo(u64 address) {
		while (memory.size() < address) {
			memory.push_back(0);
		}
	}

	void setup(std::vector<u32> instructions) {
		memory.clear();

		u64 acc = 0;
		for (int i = 0; i < instructions.size(); i++) {
			acc = (acc << 32) + (u64)instructions[i];

			// Second of the pair, insert to memory
			if (i % 2 == 1) {
				memory.push_back(acc);
				acc = 0;
			}
		}

		// Odd number of half words, add last one
		if (instructions.size() % 2 == 1) {
			memory.push_back(acc << 32);
		}
	}

	void resetStats() {
		modified_words.clear();
		read_words.clear();
	}
};

struct FeatureFlags {
	enum BPType {
		NEVER_TAKE,
		ALWAYS_TAKE,
		DYNAMIC,
		STATIC
	};

	bool pipelining = true;
	bool super = true;
	bool speculative = true;
	BPType bp_type = DYNAMIC;
};



};
