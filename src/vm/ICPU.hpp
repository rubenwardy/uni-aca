#pragma once

#include <string>
#include <utility>
#include <vector>
#include "../utils.hpp"

namespace VM {
	class VM;
}

class ICPU {
protected:
	VM::VM *vm;
public:
	explicit ICPU(VM::VM *vm):
		vm(vm)
	{}

	virtual ~ICPU() {}
	virtual void step() = 0;
	virtual void clear() = 0;
	virtual bool isRunnable() const;
	virtual std::string getCPUName() const = 0;

	static ICPU *create(VM::VM *vm);

	struct Mark {
		Mark(std::string name, int pc, std::string value,
			 std::string color):
			 name(std::move(name)), pc(pc), value(std::move(value)),
			 color(std::move(color))
	 	{}

		std::string name;
		int pc;
		std::string value;
		std::string color;
	};

	virtual bool hasMarks() const { return false; }
	virtual std::vector<Mark> getMarks() const { return std::vector<Mark>(); }
};
