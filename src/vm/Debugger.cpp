#include "Debugger.hpp"
#include "src/asm/Assembler.hpp"

#include <iostream>
#include <sstream>
#include <cassert>

using namespace VM;

#define TB_START 0
#define TB_STEP  1
#define TB_PAUSE 2
#define TB_STOP  3
#define TB_LOAD  4

const Glib::ustring ui_info =
	 "<interface>"
	 "  <requires lib='gtk+' version='3.4'/>"
	 "  <object class='GtkToolbar' id='toolbar'>"
	 "    <property name='visible'>True</property>"
	 "    <property name='can_focus'>False</property>"
	 "    <child>"
	 "      <object class='GtkToolButton' id='toolbutton_start'>"
	 "        <property name='visible'>True</property>"
	 "        <property name='can_focus'>False</property>"
	 "        <property name='tooltip_text' translatable='yes'>Start</property>"
	 "        <property name='action_name'>app.start</property>"
	 "        <property name='icon_name'>media-playback-start</property>"
	 "      </object>"
	 "      <packing>"
	 "        <property name='expand'>False</property>"
	 "        <property name='homogeneous'>True</property>"
	 "      </packing>"
	 "    </child>"
	 "    <child>"
 	 "      <object class='GtkToolButton' id='toolbutton_step'>"
 	 "        <property name='visible'>True</property>"
 	 "        <property name='can_focus'>False</property>"
 	 "        <property name='tooltip_text' translatable='yes'>Step</property>"
 	 "        <property name='action_name'>app.step</property>"
 	 "        <property name='icon_name'>go-next</property>"
 	 "      </object>"
 	 "      <packing>"
 	 "        <property name='expand'>False</property>"
 	 "        <property name='homogeneous'>True</property>"
 	 "      </packing>"
 	 "    </child>"
	 "    <child>"
	 "      <object class='GtkToolButton' id='toolbutton_pause'>"
	 "        <property name='visible'>True</property>"
	 "        <property name='can_focus'>False</property>"
	 "        <property name='tooltip_text' translatable='yes'>Pause</property>"
	 "        <property name='action_name'>app.pause</property>"
	 "        <property name='icon_name'>media-playback-pause</property>"
	 "      </object>"
	 "      <packing>"
	 "        <property name='expand'>False</property>"
	 "        <property name='homogeneous'>True</property>"
	 "      </packing>"
	 "    </child>"
	 "    <child>"
	 "      <object class='GtkToolButton' id='toolbutton_stop'>"
	 "        <property name='visible'>True</property>"
	 "        <property name='can_focus'>False</property>"
	 "        <property name='tooltip_text' translatable='yes'>Stop</property>"
	 "        <property name='action_name'>app.stop</property>"
	 "        <property name='icon_name'>media-playback-stop</property>"
	 "      </object>"
	 "      <packing>"
	 "        <property name='expand'>False</property>"
	 "        <property name='homogeneous'>True</property>"
	 "      </packing>"
	 "    </child>"
	 "    <child>"
	 "      <object class='GtkToolButton' id='toolbutton_load'>"
	 "        <property name='visible'>True</property>"
	 "        <property name='can_focus'>False</property>"
	 "        <property name='tooltip_text' translatable='yes'>Load</property>"
	 "        <property name='action_name'>app.load</property>"
	 "        <property name='icon_name'>document-open</property>"
	 "      </object>"
	 "      <packing>"
	 "        <property name='expand'>False</property>"
	 "        <property name='homogeneous'>True</property>"
	 "      </packing>"
	 "    </child>"
	 "  </object>"
	 "</interface>";


Debugger::Debugger(VM *vm):
	vm   (vm),
	split(Gtk::ORIENTATION_HORIZONTAL),
	split2(Gtk::ORIENTATION_HORIZONTAL),
	info (Gtk::ORIENTATION_VERTICAL),
	fRegs("Register File"),
	lRegs("loading"),
	fClock("Performance"),
	bxClock(Gtk::ORIENTATION_VERTICAL),
	lTick("Tick: 0"),
	fPipeline("Pipeline"),
	lPipeline("loading")
{
	assert(vm);

	set_title("ACA GUI Debugger");
	set_default_size(860, 640);
	move(100, 50);

	add(split);

	tbActionGroup = Gio::SimpleActionGroup::create();

	tbActionGroup->add_action("quit",  sigc::mem_fun(*this, &Debugger::quit));
	tbActionGroup->add_action("start", sigc::mem_fun(*this, &Debugger::resume));
	tbActionGroup->add_action("step",  sigc::mem_fun(*this, &Debugger::step));
	tbActionGroup->add_action("pause", sigc::mem_fun(*this, &Debugger::pause));
	tbActionGroup->add_action("stop",  sigc::mem_fun(*this, &Debugger::stop));
	tbActionGroup->add_action("load",  sigc::mem_fun(*this, &Debugger::onLoadClicked));
	tbActionGroup->add_action("togglerun", sigc::mem_fun(*this, &Debugger::onToggleRun));
	insert_action_group("app", tbActionGroup);

	refBuilder = Gtk::Builder::create();
	refBuilder->add_from_string(ui_info);
	refBuilder->get_widget("toolbar", toolbar);
	info.pack_start(*toolbar, Gtk::PACK_SHRINK);

	split.add1(info);

	fRegs.add(lRegs);
	fRegs.set_border_width(10);
	info.pack_start(fRegs, Gtk::PACK_SHRINK);

	bxClock.pack_start(lTick, Gtk::PACK_SHRINK);
	bxClock.pack_start(sClock, Gtk::PACK_SHRINK);
	sClock.set_range(1, 100);
	sClock.set_value(10);
	sClock.set_digits(0);
	fClock.add(bxClock);
	fClock.set_border_width(10);
	info.pack_start(fClock, Gtk::PACK_SHRINK);

	sClock.signal_change_value().connect(sigc::mem_fun(this, &Debugger::onClockChanged));

	fPipeline.add(lPipeline);
	fPipeline.set_border_width(10);
	info.pack_start(fPipeline, Gtk::PACK_SHRINK);

	//
	// Instructions
	//

	scrollInstructions.add(treeViewInstructions);
	scrollInstructions.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	split2.add1(scrollInstructions);

	refTreeModelInstructions = Gtk::ListStore::create(colsInstructions);
	treeViewInstructions.set_model(refTreeModelInstructions);
	createInstRows();

	treeViewInstructions.append_column_numeric("PC",   colsInstructions.pc,   "%02x");
	treeViewInstructions.append_column_numeric("Inst", colsInstructions.inst, "%08x");
	treeViewInstructions.append_column("Op", colsInstructions.op);
	treeViewInstructions.append_column("Operands", colsInstructions.operands);

	for (int i = 0; i < 4; i++) {
		Gtk::TreeViewColumn* column = treeViewInstructions.get_column(i);
		if (column) {
			Gtk::CellRenderer *cellrenderer = column->get_first_cell();
			column->add_attribute(cellrenderer->property_cell_background(), colsInstructions.color);
		}
	}

	Glib::RefPtr<Gtk::TreeSelection> refTreeSelection = treeViewInstructions.get_selection();
	refTreeSelection->signal_changed().connect(
			sigc::mem_fun(*this, &Debugger::onInstSelectionChanged));


	//
	// Memory
	//

	scrollMemory.add(treeViewMemory);
	scrollMemory.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	split2.add2(scrollMemory);
	split2.set_position(350);
	split.add2(split2);
	split.set_position(250);

	refTreeModelMemory = Gtk::ListStore::create(colsMemory);
	treeViewMemory.set_model(refTreeModelMemory);
	createMemoryRows();

	treeViewMemory.append_column_numeric("Address", colsMemory.address, "%02x");
	treeViewMemory.append_column_numeric("A", colsMemory.dataA, "%08x");
	treeViewMemory.append_column_numeric("B", colsMemory.dataB, "%08x");

	for (int i = 0; i < 3; i++) {
		Gtk::TreeViewColumn* column = treeViewMemory.get_column(i);
		if (column) {
			Gtk::CellRenderer *cellrenderer = column->get_first_cell();
			column->add_attribute(cellrenderer->property_cell_background(), colsMemory.color);
		}
	}

	show_all_children();

	stop();
	displayState();
	onClockChanged(Gtk::SCROLL_NONE, 10);
}

bool Debugger::update() {
	if (running) {
		doVMStep();
	}
	return true;
}

void Debugger::displayState() {
	auto file = vm->peekRegisterFile();

	{
		std::ostringstream ss;

		for (int y = 0; y < 4; y++) {
			for (int x = 0; x < 4; x++) {
				int i = y + x*4;
				bool modified, read;
				u64 val = file.getRaw(i, modified, read);
				ss << i << "=" << std::hex << val << "\t";
			}
			ss << std::endl;
		}
		{
			bool modified, read;
			u64 val = file.getRaw(16, modified, read);
			ss << std::hex << 16 << "=" << val;
		}

		lRegs.set_text(ss.str());
		lRegs.set_justify(Gtk::JUSTIFY_LEFT);
	}

	{
		std::ostringstream ss;

		auto stats = vm->peekStats();

		ss << "Clock: cycles ";
		ss << stats.cycles;

		ss << " | insts ";
		ss << stats.insts;

		ss << " | I/C ";
		ss.setf(std::ios::fixed, std::ios::floatfield);
		ss.precision(1);
		ss << (float)stats.insts / (float)stats.cycles;

		ss << std::endl;

		ss << "Branch: hit ";
		ss << stats.branch_hit;

		ss << " | miss ";
		ss << stats.branch_miss;

		lTick.set_text(ss.str());
	}

	{
		std::ostringstream ss;

		auto cpu = vm->peekCPU();

		if (!cpu->hasMarks()) {
			ss << "Pipelining disabled";
		} else {
			auto marks = cpu->getMarks();

			auto children = refTreeModelInstructions->children();
			for (auto &row : children) {
				row[colsInstructions.color] = "#29353b";
			}

			for (auto &mark : marks) {
				ss << mark.name;
				ss << "[";
				ss << std::hex;
				ss << mark.pc;
				ss << "] ";
				ss << mark.value;
				ss << std::endl;

				children[mark.pc][colsInstructions.color] = mark.color;
			}
		}

		lPipeline.set_text(ss.str());
	}

	Glib::RefPtr<Gtk::TreeSelection> refTreeSelection = treeViewInstructions.get_selection();
	Gtk::TreeModel::Row row = refTreeModelInstructions->children()[file.get(rPC)]; //The fifth row.
	if (row) {
		treeViewInstructions.scroll_to_row(refTreeModelInstructions->get_path(row));
		refTreeSelection->select(row);
	} else {
		refTreeSelection->unselect_all();
	}

	updateMemoryRows();
}

void Debugger::onInstSelectionChanged() {
	if (!running) {
		Glib::RefPtr<Gtk::TreeSelection> refTreeSelection = treeViewInstructions.get_selection();
		Gtk::TreeModel::iterator iter = refTreeSelection->get_selected();
		if (iter) {
			Gtk::TreeModel::Row row = *iter;
			vm->setPC(row[colsInstructions.pc]);
			toolbar->get_nth_item(TB_START)->set_sensitive(true);
			toolbar->get_nth_item(TB_STEP) ->set_sensitive(true);
			if (row[colsInstructions.pc] > 0) {
				toolbar->get_nth_item(TB_STOP)->set_sensitive(true);
			}
		}
	}

	displayState();
}

void Debugger::createInstRows() {
	auto source = vm->peekInstructions();

	refTreeModelInstructions->clear();

	for (int i = 0; i < source.size(); i++) {
		u32 inst = source[i];

		Gtk::TreeModel::Row row = *(refTreeModelInstructions->append());
		row[colsInstructions.pc]       = i;
		row[colsInstructions.inst]     = inst;
		row[colsInstructions.op]       = Inst(inst).getOpName();
		row[colsInstructions.operands] = Inst(inst).getOperands();
	}
}

void Debugger::updateMemoryRows() {
	auto memoryUnit = vm->peekMemory();
	auto memory     = memoryUnit.peek();

	if (memory.size() != numMemoryRows) {
		createMemoryRows();
		return;
	}

	bool before = isMemoryColored;
	isMemoryColored = !(memoryUnit.read_words.empty() && memoryUnit.modified_words.empty());
	if (!before && !isMemoryColored) {
		return;
	}

	auto children = refTreeModelMemory->children();
	for (u64 i = 0; i < memory.size(); i++) {
		u64 data = memory[i];
		if (memoryUnit.modified_words.find(i) != memoryUnit.modified_words.end()) {
			children[i][colsMemory.dataA] = data >> 32;
			children[i][colsMemory.dataB] = data &  0xFFFFFFFF;
			children[i][colsMemory.color] = "#b34040";
		} else if (memoryUnit.read_words.find(i) != memoryUnit.read_words.end()) {
			children[i][colsMemory.color] = "#0082d4";
		} else {
			children[i][colsMemory.color] = "#29353b";
		}
	}
}

void Debugger::createMemoryRows() {
	auto memoryUnit = vm->peekMemory();
	auto memory     = memoryUnit.peek();
	numMemoryRows   = memory.size();

	refTreeModelMemory->clear();

	for (u64 i = 0; i < memory.size(); i++) {
		u64 data = memory[i];

		Gtk::TreeModel::Row row = *(refTreeModelMemory->append());
		row[colsMemory.address] = i * 2;
		row[colsMemory.dataA]   = data >> 32;
		row[colsMemory.dataB]   = data &  0xFFFFFFFF;

		if (memoryUnit.modified_words.find(i) != memoryUnit.modified_words.end()) {
			row[colsMemory.color] = "#b34040";
		} else if (memoryUnit.read_words.find(i) != memoryUnit.read_words.end()) {
			row[colsMemory.color] = "#0082d4";
		}
	}
}

void Debugger::resume() {
	if (running || !vm->isRunnable()) {
		return;
	}

	std::cerr << "start!" << std::endl;
	toolbar->get_nth_item(TB_START)->set_sensitive(false);
	toolbar->get_nth_item(TB_STEP) ->set_sensitive(false);
	toolbar->get_nth_item(TB_PAUSE)->set_sensitive(true);
	toolbar->get_nth_item(TB_STOP) ->set_sensitive(true);
	toolbar->get_nth_item(TB_LOAD) ->set_sensitive(false);
	running = true;
}

void Debugger::step() {
	if (running || !vm->isRunnable()) {
		return;
	}

	doVMStep();
}

void Debugger::pause() {
	if (!running) {
		return;
	}

	std::cerr << "pause!" << std::endl;
	toolbar->get_nth_item(TB_START)->set_sensitive(vm->isRunnable());
	toolbar->get_nth_item(TB_STEP) ->set_sensitive(vm->isRunnable());
	toolbar->get_nth_item(TB_PAUSE)->set_sensitive(false);
	toolbar->get_nth_item(TB_LOAD) ->set_sensitive(true);
	running = false;
}

void Debugger::stop() {
	std::cerr << "stop!" << std::endl;
	toolbar->get_nth_item(TB_START)->set_sensitive(true);
	toolbar->get_nth_item(TB_STEP) ->set_sensitive(true);
	toolbar->get_nth_item(TB_PAUSE)->set_sensitive(false);
	toolbar->get_nth_item(TB_STOP) ->set_sensitive(false);
	toolbar->get_nth_item(TB_LOAD) ->set_sensitive(true);
	running = false;

	vm->reset();
	displayState();
}

void Debugger::onLoadClicked() {
	if (running) {
		return;
	}

	Gtk::FileChooserDialog dialog("Please choose a file", Gtk::FILE_CHOOSER_ACTION_OPEN);
	dialog.set_transient_for(*this);

	// Add response buttons the the dialog:
	dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
	dialog.add_button("_Open",   Gtk::RESPONSE_OK);

	// Add filters, so that only certain file types can be selected
	auto filter_asm = Gtk::FileFilter::create();
	filter_asm->set_name("Assembly files");
	filter_asm->add_pattern("*.s");
	dialog.add_filter(filter_asm);

	auto filter_any = Gtk::FileFilter::create();
	filter_any->set_name("Any files");
	filter_any->add_pattern("*");
	dialog.add_filter(filter_any);

	if (dialog.run() == Gtk::RESPONSE_OK) {
		ASM::Assembler as;
		if (!as.parseFile(dialog.get_filename())) {
			return;
		}

		std::vector<u32> source = as.getInstructions();
		vm->setInstructions(source);
		createInstRows();
		displayState();
	}
}

bool Debugger::onClockChanged(Gtk::ScrollType, double) {
	u32 val = (u32)sClock.get_value();

	tick_conn.disconnect();
	tick_conn = Glib::signal_timeout().connect(
				sigc::mem_fun(this, &Debugger::update), 1000 / val);

	return false;
}

void Debugger::doVMStep() {
	try {
		if (!vm->step()) {
			pause();
		}
		vm->printDebug();
		if (!vm->isRunnable()) {
			pause();
		}
		displayState();
	} catch (VMException &vme) {
		std::cerr << vme.what() << std::endl;

		pause();
		displayState();

		Gtk::MessageDialog dialog(*this, "VM Error");
		dialog.set_secondary_text(vme.what());
		dialog.run();
	}
}
