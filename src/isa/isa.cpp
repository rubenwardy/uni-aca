#include "isa.hpp"

#include <sstream>
#include <string.h>
#include <cassert>
#include <iostream>

using namespace ISA;

Inst::Inst(u32 inst) {
	op = (Op)((inst >> 28) & 0xF);

	switch (op) {
	case ADD:
	case CMP:
	case MUL:
	case DIV:
		a = (inst >> 24) & 0xF;
		b = (inst >> 20) & 0xF;
		c = (inst >> 16) & 0xF;
		break;

	case ADDI:
	case LDR:
	case STR:
	case B:
		a   = (inst >> 24) & 0xF;
		b   = (inst >> 20) & 0xF;
		imm = inst & 0xFFFFF;
		break;

	case NOP:
		break;
	}

	if (imm & 0x80000) {
		std::cerr << "Negative! " << (s32)imm << std::endl;
		imm |= 0xFFF00000;
		assert((s32)imm < 0);
	}
}

InstType Inst::getType() const {
	switch (op) {
	case ADD:
	case CMP:
	case MUL:
	case DIV:
		return IT_RRR;

	case ADDI:
	case LDR:
	case STR:
	case B:
		return IT_RRI;

	case NOP:
		return IT_NULL;
	default:
		throw("Unknown OP");
	}
}

u32 Inst::encode() const {
	u32 ret = ((u32)op) << 28;

	switch (getType()) {
	case IT_NULL:
		break;

	case IT_RRI:
		ret |= ((u32)a) << 24;
		ret |= ((u32)b) << 20;

		if (!CheckIMM(imm)) {
			throw("Immediate out of range");
		}

		ret |= imm & 0x000FFFFF;
		break;

	case IT_RRR:
		ret |= ((u32)a) << 24;
		ret |= ((u32)b) << 20;
		ret |= ((u32)c) << 16;
		break;

	default:
		throw("Unknown OP");
	}

	return ret;
}

std::string Inst::getOpName() const {
	std::ostringstream os;

	os << OP_TO_NAME[op];

	if (op == B) {
		switch (a) {
		case 0:
			break;
		case 1:
			os << "z";
			break;
		case 2:
			os << "nz";
			break;
		case 3:
			os << "p";
			break;
		case 4:
			os << "n";
			break;
		default:
			throw "This should never happen!";
		}
	}
	return os.str();
}

std::string Inst::getOperands() const {
	std::ostringstream os;

	if (op == B) {
		if (b != rZR) {
			os << "r" << (int)b << ", ";
		}
		os << "#" << std::hex << (int)imm;
		return os.str();
	}

	switch (getType()) {
	case IT_NULL:
		break;

	case IT_RRI:
		os << "r" << (int)a;
		os << ", r" << (int)b;
		os << ", #" << std::hex << (int)imm;
		break;

	case IT_RRR:
		os << "r" << (int)a;
		os << ", r" << (int)b;
		os << ", r" << (int)c;
		break;

	default:
		throw("Unknown OP");
	}

	return os.str();
}

std::string Inst::toString() const {
	std::ostringstream os;
	os << getOpName() << " " << getOperands();
	return os.str();
}

signed char Inst::getWriteRegister() const {
	switch (op) {
	case ADD:
	case ADDI:
	case LDR:
	case MUL:
	case DIV:
		return a;

	case CMP:
		return rCOND;

	case STR:
	case B:
	case NOP:
		return -1;
	}
}

std::array<signed char, 2> Inst::getReadRegisters() const {
	switch (op) {
	case ADD:
	case MUL:
	case DIV:
		return { b, c };

	case CMP:
		return { a, b };

	case ADDI:
	case LDR:
		return { b, -1 };

	case STR:
		return { b, a };

	case B:
		return { (signed char)(a ? rCOND : -1), b };

	case NOP:
		return { -1, -1 };
	}
}
