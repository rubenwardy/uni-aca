#pragma once

#include <array>
#include "../utils.hpp"

namespace ISA {

static const int LOAD_CYCLES = 3;
static const int STORE_CYCLES = 0;

enum Reg {
	r0 = 0,
	r1,
	r2,
	r3,
	r4,
	r5,
	r6,
	r7,
	r8,
	r9,
	r10,
	r11,
	rZR,
	rSP,
	rLR,
	rPC,
	rCOND,
};

enum Op {
	NOP = 0,
	ADD,
	ADDI,
	CMP,
	LDR,
	STR,
	MUL,
	DIV,
	B = 0xB
};

static const char *OP_TO_NAME[] = {
		"nop",
		"add",
		"addi",
		"cmp",
		"ldr",
		"str",
		"mul",
		"div",
		"",
		"",
		"",
		"b"
	};

enum InstType {
	IT_NULL,
	IT_RRI,
	IT_RRR
};

class Inst {
public:
	explicit Inst(u32 inst);
	Inst(): Inst(0) {}

	Op op = NOP;

	// Operands
	char a = 0;
	char b = 0;
	union {
		char c;
		u32 imm = 0;
	};

	InstType getType() const;
	u32 encode() const;
	std::string getOpName() const;
	std::string getOperands() const;
	std::string toString() const;
	signed char getWriteRegister() const;
	std::array<signed char, 2> getReadRegisters() const;

	u64 getIMM64() const {
		if ((imm & 0xFFF80000) == 0xFFF80000) {
			return (u64)imm | 0xFFFFFFFF00000000;
		} else {
			return imm;
		}
	}

	inline static bool CheckIMM(u32 imm) {
		u32 val = imm & 0xFFF00000;
		return val == 0 || val == 0xFFF00000;
	}
};

};
