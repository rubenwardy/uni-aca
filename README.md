# ACA VM

## Getting started

Dependencies:

* C++11 (eg: GCCv5+)
* gtkmm3, glibmm3, gtk3

Installation on Ubuntu:

	sudo apt install build-essential libgtkmm-3.0-dev

Compilation:

	cmake .
	make -j9

## Usage

### bin/a

	./bin/a [flags] [path/to/file]

Flags:

* `--cli`, `-c` - Enable CLI mode. Defaults to GUI.
* `--asm`, `-a` - Assemble file and quit.
* `--help`, `-h` - Show help.
* `--no-pipelining`, `-p` - Disable pipeline mode.

### utils/runall.sh

Runs all benchmarks and generates performance reports.

### utils/run-and-check.sh

Generates performance report for single benchmark.

	./utils/run-and-check.sh name
