#!/bin/bash

mkdir -p reports
cd reports


collect() {
    for file in *.$1
    do
        [[ ${file} == test_* ]] && continue
        line=$(sed ':a;N;$!ba;s/\n/   /g' "$file")
        file=${file%.$1}
        echo "\"$file\"   $line" >> $2
    done
}


collect_tests() {
    for file in test_*.$1
    do
        line=$(sed ':a;N;$!ba;s/\n/   /g' "$file")
        file=${file%.$1}
        echo "\"$file\"   $line" >> $2
    done
}

#Title   Simple   Pipelined   Superscalar
#"Writing code"  6   4   10
#"Understanding code"    6   3   4
#"Generating prime numbers"  3   1   0

cd data

echo "Title   Simple   Pipelined   SuperDynBP   SuperStBP   SuperNvrBP   SuperAlBP   SuperNoBP" > ../perf.data
collect "ipc.txt" ../perf.data


echo "Title   Simple   Pipelined   SuperDynBP   SuperStBP   SuperNvrBP   SuperAlBP   SuperNoBP" > ../perf2.data
collect "ipc.txt" ../perf2.data
collect_tests "ipc.txt" ../perf2.data


echo "Title   Err   Err   Dynamic   Static   Never   Always   None" > ../bp.data
collect "bp.txt" ../bp.data
#collect_tests "bp.txt" ../bp.data

cd ../



gnuplot ../utils/performance.plt
gnuplot ../utils/performanceBP.plt
gnuplot ../utils/bp.plt
cd ..

