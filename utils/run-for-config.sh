#!/bin/bash

cd examples
for file in *.s
do
	file=${file%.*}
	cd ../
	./utils/run-and-check.sh "$file" $@
	cd examples
done
