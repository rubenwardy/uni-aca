set title "Branch Prediction Rates"
Simple = "#ff0000"; Pipelined = "#ff0000"; Superscalar = "#ff0000"
#Simple = "#FF9800"; Pipelined = "#2196F3"; Superscalar = "#673AB7"
set auto x
set yrange [0:100]
set ylabel "Branch Hit Rate (%)"
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
set boxwidth 0.9
set xtic scale 0
set key outside center bottom horizontal
set terminal png noenhanced
set output 'bp.png'
set xtics rotate by -45 offset -0.8,0
# 2, 3, 4, 5 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot 'bp.data' using 6:xtic(1) ti col, '' u 7 ti col, '' u 5 ti col, '' u 4 ti col
