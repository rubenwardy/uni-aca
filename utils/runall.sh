#!/bin/bash

rm -rf reports/databk
mv reports/data reports/databk
mkdir -p reports/data

echo "=== SIMPLE ==="
./utils/run-for-config.sh --simple      || exit $!

echo ""
echo "=== PIPELINED ==="
./utils/run-for-config.sh --pipelined    || exit $!

echo ""
echo "=== SUPER-SCALAR DYNAMIC-BP ==="
./utils/run-for-config.sh --superscalar --dynamic-bp || exit $!

echo ""
echo "=== SUPER-SCALAR STATIC-BP ==="
./utils/run-for-config.sh --superscalar --static-bp || exit $!

echo ""
echo "=== SUPER-SCALAR NEVER-TAKE ==="
./utils/run-for-config.sh --superscalar --never-bp || exit $!

echo ""
echo "=== SUPER-SCALAR ALWAYS-TAKE ==="
./utils/run-for-config.sh --superscalar --always-bp || exit $!

echo ""
echo "=== SUPER-SCALAR NO-BP ==="
./utils/run-for-config.sh --superscalar --no-bp || exit $!



./utils/gen-reports.sh || exit 10
if [[ "$1" = "-r" ]]; then
    xdg-open reports/performance.png
fi
