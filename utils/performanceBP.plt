set title "IPCs for different branch predictors, vs Simple"
Simple = "#ff0000"; Pipelined = "#ff0000"; Superscalar = "#ff0000"
#Simple = "#FF9800"; Pipelined = "#2196F3"; Superscalar = "#673AB7"
set auto x
set yrange [0:200]
set ylabel "Instructions Per Cycle (%)"
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
set boxwidth 0.9
set xtic scale 0
set key outside center bottom horizontal
set terminal png noenhanced
set output 'performance_bp.png'
set xtics rotate by -45 offset -0.8,0
set arrow from graph 0,first 100 to graph 1,first 100 nohead lc rgb "#bbbbbb" front
# 2, 3, 4, 5 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot 'perf.data' using 2:xtic(1) ti col, '' u 8 ti col, '' u 7 ti col, '' u 6 ti col, '' u 5 ti col, '' u 4 ti col
