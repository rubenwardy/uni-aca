#!/bin/bash

if [[ "$#" -lt 1 ]]; then
	echo "USAGE: run-and-check TEST_NAME ARGS..."
	exit 1
fi

src=examples/$1.s
exp=examples/$1.exp
data=reports/data/

CLEAR="\033[0m"
RED="\033[;31m"
GREEN="\033[;32m"
PROG="./bin/a --cli ${@:2} ${src}"


if ! error=$(($PROG > /tmp/aca; ) 2>&1 ); then
	if echo $error | grep -iq "Timed out"; then
        printf "$RED%-15s [ TIMEOUT ]$CLEAR\n" $1
	else
	    printf "$RED%-15s [ FAILED ]$CLEAR\n" $1
	fi
	echo "0" >> $data/$1.ipc.txt
	echo "0" >> $data/$1.bp.txt
	exit 1
fi

result=$(cat /tmp/aca)

ipc=$(echo ${result} | grep -oP 'ipc = \K\w+')
bperc=$(echo ${result} | grep -oP 'branch_perc = \K\w+')
bhit=$(echo ${result} | grep -oP 'branch_hit = \K\w+')
bmiss=$(echo ${result} | grep -oP 'branch_miss = \K\w+')

echo $ipc >> $data/$1.ipc.txt
echo $bperc >> $data/$1.bp.txt

if [[ -f "$exp" ]]; then
	while read cond; do
		if !(grep -q "^$cond$" <<< "$result"); then
			printf "$RED%-15s [ FAILED ]   %s$CLEAR\n" $1 $cond
			exit 1
		fi
	done < $exp

	printf "$GREEN%-15s [ OK ]$CLEAR   IPC=%3s   B=%3s (BHIT=%3s, BMISS=%3s)\n" $1 $ipc $bperc $bhit $bmiss
else
	printf "$GREEN%-15s [ NT ]$CLEAR   IPC=%3s,  B=%3s (BHIT=%3s, BMISS=%3s)\n" $1 $ipc $bperc $bhit $bmiss
fi
